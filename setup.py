#!/usr/bin/env python
import os

if __name__ == '__main__':
    # creates folders for training and test data
    training_dir = os.path.join(os.path.dirname(__file__), 'training_data')
    if not os.path.exists(training_dir):
        os.makedirs(training_dir)
    # folder rosbags
    rosbags = "%s/rosbags" % (training_dir,)
    if not os.path.exists(rosbags):
        os.makedirs(rosbags)
    # subfolder rosbags/test
    rosbags = "%s/rosbags/test" % (training_dir,)
    if not os.path.exists(rosbags):
        os.makedirs(rosbags)
    # subfolder rosbags/train
    rosbags = "%s/rosbags/train" % (training_dir,)
    if not os.path.exists(rosbags):
        os.makedirs(rosbags)
    # folder data
    data = "%s/data" % (training_dir,)
    if not os.path.exists(data):
        os.makedirs(data)
    # subfolder data/test
    data = "%s/data/test" % (training_dir,)
    if not os.path.exists(data):
        os.makedirs(data)
    # subfolder data/train
    data = "%s/data/train" % (training_dir,)
    if not os.path.exists(data):
        os.makedirs(data)

    # creates folder for trained data
    trained_data = os.path.join(os.path.dirname(__file__), 'trained_data')
    if not os.path.exists(trained_data):
        os.makedirs(trained_data)

    # creates folder for graphs
    graphs = os.path.join(os.path.dirname(__file__), 'graphs')
    if not os.path.exists(graphs):
        os.makedirs(graphs)

    # creates folder for logs
    logs = os.path.join(os.path.dirname(__file__), 'logs')
    if not os.path.exists(logs):
        os.makedirs(logs)

    # creates folder for vis
    records = os.path.join(os.path.dirname(__file__), 'vis')
    if not os.path.exists(records):
        os.makedirs(records)
    # subfolder raw
    raw = "%s/raw" % (records,)
    if not os.path.exists(raw):
        os.makedirs(raw)
    # subfolder vis
    vis = "%s/layer" % (records,)
    if not os.path.exists(vis):
        os.makedirs(vis)
