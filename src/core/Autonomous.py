#!/usr/bin/env python
from time import localtime

import numpy as np
import rospy
from keras import backend as k
from sensor_msgs.msg import CompressedImage
from std_msgs.msg import Bool

import os
import sys
sys.path.append(os.path.dirname(
                    os.path.dirname(
                        os.path.dirname(
                            os.path.dirname(os.path.abspath(__file__))
                        )
                    )
                ))

from neuroracer_utils.CvBridgeHandler import *

class Autonomous:
    '''
    Class which contains all needed ai parameters
    '''

    def __init__(self, model, img_processor, vis_processor=None, file_writer=None,
                 cam_topic='/camera/front/image_rect_color/compressed', cam_img_type='compr',
                 visualize=False, verbose=None, record=False):

        '''
        Constructor

        :param model:         BaseModel pretrained model for the predictions
        :param img_processor: ImageProcessor used to do the image preprocessing for incoming images
        :param vis_processor: VisualizationProcessor used to visualize data, if None no visualiziation
        :param file_writer:   FileWriter used for logging tasks, if None no logging in a file
        :param cam_topic:     String the topic of the cam node
        :param cam_img_type:  String used to know which type of image gets published
        :param visualize:     Bool used to determine if visualiziation should be done
        :param verbose:       Int used for verbose logging, 0 is off, 1 is console rosinfo logging
                                                            2 is file_writer logging
        :param record:        Bool used to record preprocessed cam images
        '''

        self.model = model  # NN model
        self.img_processor = img_processor
        self.vis_processor = vis_processor
        self.file_writer = file_writer  # file handler

        self.cam_topic = cam_topic
        self.cam_img_type = cam_img_type

        self.visualize = visualize
        self.verbose = verbose  # log information
        self.record = record # record raw processed images

        self.drive_control = None  # Handler to create robot control commands
        self.drive_control_publisher = None  # ROS publisher for predicted control commands

        self.active = False  # prediction state

        self.cv_bridge_handler = CvBridgeHandlerCompr() if self.cam_img_type == 'compr' else CvBridgeHandlerRaw()

    def _callback_camera(self, msg, args):
        """
        Callback function when input data from image topic is received.
        Processes input pending on backend and predicts command

        :param msg:  ImageMsg incomig image msg
        :param args: Lambda function prediction function
        """
        if self.active:
            predict = args['predict']

            image = self.cv_bridge_handler.get_image(msg)
            image = self.img_processor.execute_img_processing(image)

            corresponding_img_name = None
            if self.record and self.file_writer is not None:
                corresponding_img_name = self.file_writer.write_image(image, 'vis/raw', corresponding_img_name)


            if self.visualize and self.vis_processor is not None:
                # NOTE: debug Lillicrap
                # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug1')
                # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug2')
                # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug3')
                # # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'flattened_observation')
                # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug5')
                # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug6')

                # NOTE: debug NVIDIA
                # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug1')
                # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug2')
                # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug3')
                # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug4')
                # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug5')
                # # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'flattened_observation')
                # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug7')
                # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug8')
                # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug9')
                # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug10')

                # NOTE: use this in general
                self.vis_processor.visualize_saliency(image, corresponding_img_name, 'prediction')


            if k.image_dim_ordering() == 'tf':
                # Tensorflow-Backend
                with self.model.graph.as_default():
                    steer, speed = np.squeeze(predict(np.array([image])))
            else:
                # Theano-Backend:
                # steer, speed = np.squeeze(predict(np.array([image])))
                pass

            command = self.drive_control.create_command(steer=steer, speed=speed)
            self.drive_control_publisher.publish(command)

            if self.verbose == 1:
                rospy.loginfo("steering: %s\t| speed: %s" % ("{:10.8f}".format(steer), "{:10.8f}".format(speed),))
            elif self.verbose == 2 and self.file_writer is not None:
                log_text = "(%s) - steering: %s\t| speed: %s" % (
                    localtime(), "{:10.8f}".format(steer), "{:10.8f}".format(speed),)
                self.file_writer.write_log_text(name=self.model.model_name, log_text=log_text, mode="autonomous")

    def _callback_state(self, bool_msg):
        '''
        Callback function for state msgs

        :param bool_msg: BoolMsg holds the new ai state
        '''

        if bool_msg.data is True or bool_msg.data is False:
            self.active = bool_msg.data

            if self.drive_control is not None and self.drive_control_publisher is not None:
                command = self.drive_control.stop_engine()
                self.drive_control_publisher.publish(command)

    def set_state(self, active=False):
        """
        Set prediction state. If deactivated no prediction commands are generated nor published.

        :param active: boolean
        """
        self.active = active

    def stop(self):
        """
        Send command to stop robot engine.
        """
        self.active = False

        if self.drive_control is not None and self.drive_control_publisher is not None:
            command = self.drive_control.stop_engine()
            self.drive_control_publisher.publish(command)

    def start(self, queue_size=1, max_speed=0.0, wait_for_input=True, is_simulation=True):
        """
        Start autonomous mode.

        :param queue_size:     Int queue_size value
        :param max_speed:      Float max value for speed values
        :param wait_for_input: Bool if True waits before start for user input
        :param is_simulation:  Bool if True uses AckermannMsgs else TwistMsgs
        """
        # a switch, to start required ROS publisher handling simulation or robot
        if is_simulation:
            from DriveControl import DriveControlSimulation
            from ackermann_msgs.msg import AckermannDriveStamped

            self.drive_control = DriveControlSimulation(max_speed)
            cam_path = self.cam_topic
            output_path = 'vesc/ackermann_cmd_mux/input/navigation'
            drive_control_publisher = rospy.Publisher(output_path,
                                                      AckermannDriveStamped,
                                                      queue_size=queue_size)
        else:
            from DriveControl import DriveControlRacer
            from geometry_msgs.msg import Twist

            self.drive_control = DriveControlRacer(max_speed)
            cam_path = self.cam_topic
            output_path = '/neuroracer_ai/output/drive_command'
            drive_control_publisher = rospy.Publisher(output_path,
                                                      Twist,
                                                      queue_size=queue_size)

        if drive_control_publisher is not None:
            self.drive_control_publisher = drive_control_publisher

        # initialize ROS AI node
        rospy.init_node('neuroracer_ai', anonymous=False)

        # ROS subscriber for ai state
        rospy.Subscriber('neuroracer_ai/input/ai_state', Bool, self._callback_state)

        # ROS subscriber for camera topic
        rospy.Subscriber(cam_path,
                         CompressedImage,
                         self._callback_camera,
                         callback_args={
                             'predict': self.model.make_stateful_predictor(),
                         })

        # Stop robot when AI is completely turned off
        rospy.on_shutdown(self.stop)

        # Wait for user input before passing control to AI
        if wait_for_input:
            raw_input("Press Enter to start")

        # Activate prediction state
        self.active = False # True

        rospy.spin()
