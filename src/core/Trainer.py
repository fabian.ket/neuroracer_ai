#!/usr/bin/env python

from os import path, makedirs
from posixpath import normpath

from core.BaseModel import RegressionModel
from keras import backend as k
from keras.callbacks import ModelCheckpoint, TensorBoard
from keras.preprocessing.image import ImageDataGenerator

class Trainer:
    '''
    Class which contains all needed training parameters
    '''

    def __init__(self, model,
                 x_train, y_train,
                 x_test,  y_test,
                 batch_size = 120, epochs = 10,
                 do_data_augmentation   = False,
                 log_nn_structure       = False,
                 log_performance        = False,
                 log_tensor_board       = False,
                 file_writer            = None,
                 verbose = 0):

        '''
        Constructor

        :param model: BaseModel pretrained model for predictions
        :param x_train: np.array<float> training input dataset has to be preprocessed for the model
        :param y_train: np.array<float> training output dataset
        :param x_test:  np.array<float> validation input dataset has to be preprocessed for the model,
                                        if None no validation
        :param y_test:  np.array<float> validation output dataset, if None no validation
        :param batch_size: Int training batch size
        :param epochs:     Int epochs to train
        :param do_data_augmentation: Bool state to say if realtime data augmentation should be used
        :param log_nn_structure:     Bool to log the nn structur with file_writer, outputs png
        :param log_performance:      Bool to log performance with file_writer
        :param log_tensor_board:     Bool to log tensor board logs with Keras callbacks
        :param file_writer: FileWriter used for logging, if None no logging
        :param verbose:     Int for logging in console, 0 is off
        '''

        self.model = model
        self.x_train = x_train
        self.y_train = y_train
        self.x_test  = x_test
        self.y_test  = y_test

        self.batch_size = batch_size
        self.epochs = epochs

        self.do_data_augmentation = do_data_augmentation
        self.log_nn_structure = log_nn_structure
        self.log_performance = log_performance
        self.log_tensor_board = log_tensor_board

        self.file_writer = file_writer

        self.verbose = verbose

    def start(self):
        '''
        Method to start the training with current parameters,
        creates ModelCheckpoint callback, if set TensorBoard callback,
        logs performance, logs nn structur and does realtime data augmentation
        '''

        print('Start training...')
        callbacks_list = []

        # checkpoint
        tmp_dir = path.dirname(__file__)
        file_path = "trained_data/%s-%s.hdf5" % (self.model.model_name, k.image_dim_ordering())
        file_path = path.join(tmp_dir, '../..', file_path)
        file_path = normpath(file_path)
        checkpoint = ModelCheckpoint(filepath=file_path,
                                     monitor='val_loss',
                                     verbose=self.verbose,
                                     save_best_only=True,
                                     mode='min')

        callbacks_list.append(checkpoint)

        if self.log_tensor_board:
            file_path = "graphs/%s" % (self.model.model_name,)
            file_path = path.join(tmp_dir, '../..', file_path)
            file_path = normpath(file_path)
            if not path.exists(file_path):
                makedirs(file_path)
            tb_call_back = TensorBoard(log_dir=file_path, histogram_freq=0, write_graph=True, write_images=True)
            callbacks_list.append(tb_call_back)

        if not self.do_data_augmentation:
            print('Not using data augmentation.')
            history = self.model.model.fit(self.x_train, self.y_train,
                                           batch_size      = self.batch_size,
                                           epochs          = self.epochs,
                                           callbacks       = callbacks_list,
                                           validation_data = (self.x_test, self.y_test),
                                           verbose         = self.verbose,
                                           shuffle         = True)
        else:
            print('Using real-time data augmentation.')

            data_gen = ImageDataGenerator(
                featurewise_center=False,  # set input mean to 0 over the data set
                samplewise_center=False,  # set each sample mean to 0
                featurewise_std_normalization=False,  # divide inputs by std of the data set
                samplewise_std_normalization=False,  # divide each input by its std
                zca_whitening=False,  # apply ZCA whitening
                rotation_range=20,  # randomly rotate data in the range (degrees, 0 to 180)
                width_shift_range=0.1,  # randomly shift data horizontally (fraction of total width)
                # height_shift_range=0.1,  # randomly shift data vertically (fraction of total height)
                shear_range=0.2,
                zoom_range=0.2,
                channel_shift_range=0.5,
                fill_mode='nearest',
                rescale=None,
                preprocessing_function=None)

            data_gen.fit(self.x_train)

            steps_per_epoch = (self.x_train.shape[0] / self.batch_size)

            history = self.model.model.fit_generator(data_gen.flow(self.x_train, self.y_train, batch_size=self.batch_size),
                                                     steps_per_epoch = steps_per_epoch,
                                                     epochs          = self.epochs,
                                                     callbacks       = callbacks_list,
                                                     validation_data = (self.x_test, self.y_test),
                                                     verbose         = self.verbose)

        if self.log_nn_structure and self.file_writer is not None:
            self.file_writer.write_nn_structure(model=self.model)

        if self.log_performance and self.file_writer is not None:
            self.file_writer.write_log_training(self.model.model_name, history)
            self.file_writer.write_log_graphs(self.model.model_name, history)

        print('Training successful.')
