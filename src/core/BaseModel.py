#!/usr/bin/env python
from yaml import load
import os
import posixpath

import numpy as np
from keras import backend as k
from keras.initializers import RandomUniform, VarianceScaling
from keras.layers import Conv2D, Dense, Dropout, Flatten, Input, concatenate, MaxPooling2D, ZeroPadding2D
from keras.models import Model, Sequential, load_model
from keras.wrappers.scikit_learn import KerasRegressor


class BaseModel(object):
    '''
    Base class for model objects
    '''

    def __init__(self):
        self.model_name = None # String for model name
        self.model = None      # Keras model
        self.graph = None      # Tensorflow graph

    def load_model(self, path=None):
        '''
        Loads model from trained_data in the project dir or the model from path

        :param path: String file path for hdf5 file

        :return Keras model from loaded file

        :raise TypeError: if no model_name is set
        :raise IOError:   if the hdf5 file can not be found
        '''

        if self.model_name is None:
            raise TypeError('Expected String. Got None type instead.')

        if path is None:
            # use real path since folder is out of /src scope
            tmp_dir = os.path.realpath(os.path.dirname(__file__))
            filename = "trained_data/%s-%s.hdf5" % (self.model_name, k.image_dim_ordering())
            filename = os.path.join(tmp_dir, "../..", filename)
            filename = posixpath.normpath(filename)
        else:
            filename = path

        if os.path.isfile(filename):
            return load_model(filename)
        else:
            raise IOError('No such file: %s' % (filename,))

    def predict_on_batch(self, x):
        '''
        predicts steering and speed value for given image

        :param x: np.array<float> model input for the prediction, the image has to be preprocessed for the model

        :return the prediction from the model

        :raise TypeError: if no model is set
        '''

        if self.model is None:
            raise TypeError('Expected type BaseModel. Got %s instead.' % (self.model,))

        x = np.array(x)

        # needed if only image mat is given, because model uses image lists
        if len(x.shape) < 4:
            x = np.array([x])

        return self.model.predict(x)

    def make_stateful_predictor(self):
        '''
        anonymous function for batch prediction

        :return the lambda function
        '''

        return lambda x: self.predict_on_batch(x)


class RegressionModel(BaseModel):
    '''
    Model class for regression tasks
    '''

    def __init__(self, model_name, nn_function, color_scale, img_cols, img_rows, img_depth, verbose=0, training=False, model_path=None):
        '''
        Constructor

        :param model_name:  String for the model name
        :param nn_function: String for model build function
        :param color_scale: String for the color_scale (mono8 or bgr8)
        :param img_cols:    Int cols of the input image
        :param img_rows:    Int rows of the input image
        :param img_depth:   Int depth of the input image
        :param verbose:     Int for verbose logging (0 == off)
        :param training:    Bool if model is used for training or not
        :param model_path:  String path to model file to load
        '''

        super(RegressionModel, self).__init__()
        self.model_name = model_name
        self.nn_function = nn_function
        self.color_scale = color_scale
        self.img_cols = img_cols
        self.img_rows = img_rows
        self.img_depth = img_depth
        self.verbose = verbose

        # Input Shape
        if k.image_dim_ordering() == 'tf':
            # Tensorflow-Backend
            self.input_shape = (self.img_rows, self.img_cols, self.img_depth)
            self.graph = k.get_session().graph
        else:
            # Theano-Backend
            # self.input_shape = (self.img_depth, self.img_rows, self.img_cols)
            # self.graph = None
            pass

        if training and model_path is None:
            self.model = KerasRegressor(build_fn=getattr(self, self.nn_function))
        else:
            self.model = self.load_model(path=model_path)

    def _steering_nvidia(self):
        '''
        NVIDIA's CNN architecture
        https://arxiv.org/abs/1604.07316

        :return Keras model
        '''

        # create
        model = Sequential()

        # Convolution no.1
        model.add(Conv2D(filters=24, kernel_size=(5, 5), strides=(2, 2), bias_initializer='he_normal', padding="valid",
                         input_shape=self.input_shape, activation='relu'))

        # Convolution no.2
        model.add(Conv2D(filters=36, kernel_size=(5, 5), strides=(2, 2), bias_initializer='he_normal', padding="valid",
                         activation='relu'))

        # Convolution no.3
        model.add(Conv2D(filters=48, kernel_size=(5, 5), strides=(2, 2), bias_initializer='he_normal', padding="valid",
                         activation='relu'))

        # Convolution no.4
        model.add(
            Conv2D(filters=64, kernel_size=(3, 3), bias_initializer='he_normal', padding="valid", activation='relu'))

        # Convolution no.5
        model.add(
            Conv2D(filters=64, kernel_size=(3, 3), bias_initializer='he_normal', padding="valid", activation='relu'))
        model.add(Flatten())

        # Fully connected no.1
        model.add(Dense(units=1164, bias_initializer='he_normal', activation='relu'))
        model.add(Dropout(0.2))

        # Fully connected no.2
        model.add(Dense(units=100, bias_initializer='he_normal', activation='relu'))
        model.add(Dropout(0.2))

        # Fully connected no.3
        model.add(Dense(units=50, bias_initializer='he_normal', activation='relu'))

        # Fully connected no.4
        model.add(Dense(units=10, bias_initializer='he_normal'))

        # Fully connected no.5
        model.add(Dense(units=1, bias_initializer='he_normal', activation='tanh', name='prediction'))

        # compile
        model.compile(loss="mse", optimizer="adam", metrics=["mse"])

        if self.verbose > 0:
            print (model.summary())

        return model

    def _steering_htw(self):
        '''
        HTW Version
        Modified version of NVIDIA approach

        :return Keras model
        '''

        S = Input(shape=self.input_shape, name='observation_input')
        c1 = Conv2D(activation='relu', filters=8, kernel_size=(5, 5), padding="valid", name='debug1',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(S)
        c2 = Conv2D(filters=12, kernel_size=(5, 5), activation='relu', name='debug2',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(c1)
        c3 = Conv2D(filters=16, kernel_size=(5, 5), activation='relu', name='debug3',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(c2)
        c4 = Conv2D(filters=24, kernel_size=(3, 3), activation='relu', name='debug4',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(c3)
        c5 = Conv2D(filters=24, kernel_size=(3, 3), activation='relu', name='debug5',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(c4)
        observation_flattened = Flatten(name='flattened_observation')(c5)
        d1 = Dense(units=256, activation='relu', name='debug7',
                   kernel_initializer='glorot_uniform')(observation_flattened)
        d1_drop = Dropout(0.2)(d1)
        d2 = Dense(units=100, activation='relu', name='debug8',
                   kernel_initializer='glorot_uniform')(d1_drop)
        d2_drop = Dropout(0.2)(d2)
        d3 = Dense(units=50, activation='relu', name='debug9',
                   kernel_initializer='glorot_uniform')(d2_drop)
        d3_drop = Dropout(0.2)(d3)
        d4 = Dense(units=10, activation='relu', name='debug10',
                   kernel_initializer='glorot_uniform')(d3_drop)
        d4_drop = Dropout(0.2)(d4)
        Steer = Dense(units=1,
                      activation='tanh',
                      name='prediction',
                      bias_initializer='zeros',
                      kernel_initializer=RandomUniform(minval=-3e-4, maxval=3e-4)
                      )(d4_drop)
        Speed = Dense(units=1,
                      activation='sigmoid',
                      bias_initializer='zeros',
                      kernel_initializer=RandomUniform(minval=0.0, maxval=3e-4)
                      )(d4_drop)
        V = concatenate([Steer, Speed], name='merge_concatenate')
        model = Model(inputs=S, outputs=V)

        # compile
        model.compile(loss='mse', optimizer='adam', metrics=['mse'])

        if self.verbose > 0:
            print (model.summary())

        return model

    def _steering_htw_tanh(self):
        '''
        HTW Version
        Modified version of NVIDIA approach

        :return Keras model
        '''

        S = Input(shape=self.input_shape, name='observation_input')
        c1 = Conv2D(activation='relu', filters=8, kernel_size=(5, 5), padding="valid", name='debug1',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(S)
        c2 = Conv2D(filters=12, kernel_size=(5, 5), activation='relu', name='debug2',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(c1)
        c3 = Conv2D(filters=16, kernel_size=(5, 5), activation='relu', name='debug3',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(c2)
        c4 = Conv2D(filters=24, kernel_size=(3, 3), activation='relu', name='debug4',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(c3)
        c5 = Conv2D(filters=24, kernel_size=(3, 3), activation='relu', name='debug5',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(c4)
        observation_flattened = Flatten(name='flattened_observation')(c5)
        d1 = Dense(units=256, activation='relu', name='debug7',
                   kernel_initializer='glorot_uniform')(observation_flattened)
        d1_drop = Dropout(0.2)(d1)
        d2 = Dense(units=100, activation='relu', name='debug8',
                   kernel_initializer='glorot_uniform')(d1_drop)
        d2_drop = Dropout(0.2)(d2)
        d3 = Dense(units=50, activation='relu', name='debug9',
                   kernel_initializer='glorot_uniform')(d2_drop)
        d3_drop = Dropout(0.2)(d3)
        d4 = Dense(units=10, activation='relu', name='debug10',
                   kernel_initializer='glorot_uniform')(d3_drop)
        d4_drop = Dropout(0.2)(d4)
        Steer = Dense(units=1,
                      activation='tanh',
                      name='prediction',
                      bias_initializer='zeros',
                      kernel_initializer=RandomUniform(minval=-3e-4, maxval=3e-4)
                      )(d4_drop)
        Speed = Dense(units=1,
                      activation='tanh',
                      bias_initializer='zeros',
                      kernel_initializer=RandomUniform(minval=-3e-4, maxval=3e-4)
                      )(d4_drop)
        V = concatenate([Steer, Speed], name='merge_concatenate')
        model = Model(inputs=S, outputs=V)

        # compile
        model.compile(loss='mse', optimizer='adam', metrics=['mse'])

        if self.verbose > 0:
            print (model.summary())

        return model

    def _steering_alex(self):
        '''
        AlexNet Version
        Modified version of AlexNet CaffeNet Version approach
        https://papers.nips.cc/paper/4824-imagenet-classification-with-deep-convolutional-neural-networks.pdf

        Code based on:
        https://dandxy89.github.io/ImageModels/alexnet/

        :return Keras model
        '''

        # Channel 1 - Convolution Net Layer 1
        S = Input(shape=self.input_shape, name='observation_input')
        c1 = Conv2D(activation='relu', filters=3, kernel_size=(11, 11), strides=(1, 1), padding="same", name='conv01',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(S)

        mp1 = MaxPooling2D(strides=(4, 4), pool_size=(4, 4))(c1)
        zp1 = ZeroPadding2D(padding=(1, 1))(mp1)

        # Channel 1 - Convolution Net Layer 2
        c2 = Conv2D(activation='relu', filters=48, kernel_size=(55, 55), strides=(1, 1), padding="same", name='conv02',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(zp1)
        mp2 = MaxPooling2D(strides=(2, 2), pool_size=(2, 2))(c2)
        zp2 = ZeroPadding2D(padding=(1, 1))(mp2)

        # Channel 1 - Convolution Net Layer 3
        c3 = Conv2D(activation='relu', filters=128, kernel_size=(27, 27), strides=(1, 1), padding="same", name='conv03',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(zp2)
        mp3 = MaxPooling2D(strides=(2, 2), pool_size=(2, 2))(c3)
        zp3 = ZeroPadding2D(padding=(1, 1))(mp3)

        # Channel 1 - Convolution Net Layer 4
        c4 = Conv2D(activation='relu', filters=192, kernel_size=(13, 13), strides=(1, 1), padding="same", name='conv04',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(zp3)
        zp4 = ZeroPadding2D(padding=(1, 1))(c4)

        # Channel 1 - Convolution Net Layer 5
        c5 = Conv2D(activation='relu', filters=192, kernel_size=(13, 13), strides=(1, 1), padding="same", name='conv05',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(zp4)
        zp5 = ZeroPadding2D(padding=(1, 1))(c5)

        # Channel 1 - Cov Net Layer 6
        c6 = Conv2D(activation='relu', filters=128, kernel_size=(27, 27), strides=(1, 1), padding="same", name='conv06',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(zp5)
        mp4 = MaxPooling2D(strides=(2, 2), pool_size=(2, 2))(c6)
        zp6 = ZeroPadding2D(padding=(1, 1))(mp4)

        # Channel 1 - Cov Net Layer 7
        observation_flattened = Flatten()(zp6)
        d1 = Dense(units=2048, activation='relu')(observation_flattened)
        d1_drop = Dropout(0.5)(d1)

        # Channel 1 - Cov Net Layer 8
        d2 = Dense(units=2048, activation='relu')(d1_drop)
        d2_drop = Dropout(0.5)(d2)

        # Final Channel - Cov Net 9
        Steer = Dense(units=1,
                      activation='tanh',
                      name='prediction',
                      bias_initializer='zeros',
                      kernel_initializer=RandomUniform(minval=-3e-4, maxval=3e-4)
                      )(d2_drop)
        Speed = Dense(units=1,
                      activation='tanh',
                      bias_initializer='zeros',
                      kernel_initializer=RandomUniform(minval=-3e-4, maxval=3e-4)
                      )(d2_drop)

        V = concatenate([Steer, Speed], name='merge_concatenate')
        model = Model(inputs=S, outputs=V)

        # compile
        model.compile(loss='mse', optimizer='adam', metrics=['mse'])

        if self.verbose > 0:
            print (model.summary())

        return model

    def _lillicrap(self):
        '''
        Approach of Reinforcement Learning DDPG algorithm (actor)
        https://arxiv.org/abs/1509.02971

        return Keras model
        '''

        S = Input(shape=self.input_shape, name='observation_input')
        c1 = Conv2D(filters=32, kernel_size=(4, 4), activation='relu', name='debug1', padding="valid",
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(S)
        c2 = Conv2D(filters=32, kernel_size=(4, 4), activation='relu', name='debug2',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(c1)
        c3 = Conv2D(filters=32, kernel_size=(4, 4), activation='relu', name='debug3',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(c2)
        c3_flatten = Flatten(name='flattened_observation')(c3)
        d1 = Dense(units=200, activation='relu', kernel_initializer='glorot_uniform', name='debug5')(c3_flatten)
        d2 = Dense(units=200, activation='relu', kernel_initializer='glorot_uniform', name='debug6')(d1)
        Steer = Dense(units=1,
                      activation='tanh',
                      name='prediction',
                      bias_initializer='zeros',
                      kernel_initializer=RandomUniform(minval=-3e-4, maxval=3e-4)
                      )(d2)
        Speed = Dense(units=1,
                      activation='sigmoid',
                      bias_initializer='zeros',
                      kernel_initializer=RandomUniform(minval=0.0, maxval=3e-4)
                      )(d2)
        V = concatenate([Steer, Speed], name='merge_concatenate')
        model = Model(inputs=S, outputs=Steer)

        model.compile(loss='mse', optimizer='adam', metrics=['mse'])

        if self.verbose > 0:
            print (model.summary())

        return model

def _main():
    import numpy as np

    width  = 256
    height = 144
    depth  = 3

    model = RegressionModel(model_name='test',
                            nn_function='_steering_alex',
                            color_scale='bgr8',
                            img_cols=width,
                            img_rows=height,
                            img_depth=depth,
                            verbose=0,
                            training=True,
                            model_path=None)

    model.model = model._steering_alex()

    image = np.ones((height,width,depth)) * 255

    while True:
        print (model.predict_on_batch(image))
        pass

if __name__ == '__main__':
    _main()
