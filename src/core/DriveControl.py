#!/usr/bin/env python
from ackermann_msgs.msg import AckermannDriveStamped
from geometry_msgs.msg import Twist


class DriveControl(object):
    '''
    Base class for drive controlls, creates command msgs and scales values
    to the correct range
    '''

    def __init__(self):
        '''
        Constructor
        '''

        self.max_speed = None           # max value for forward and backward speed
        self.zero_speed = None          # neutral speed value, racer stops
        self.zero_steering_angle = None # neutral steer value, servo middle value

    def create_command(self, steering=0.0, speed=0.0):
        '''
        Creates command msg with given steering and speed

        :param steering: Float the steering value
        :param speed:    Float the speed value

        :raise NotImplementedError
        '''

        raise NotImplementedError

    def stop_engine(self):
        '''
        Creates neutral command msg, engine stops and servo is at middle pos

        :raise NotImplementedError
        '''

        raise NotImplementedError

    @staticmethod
    def _scale_steer(steer=0.0):
        """
        Limits values to range -1.0 to 1.0

        :param steer: Float steer value to map

        :return: Float mapped value
        """
        if steer > 1.0:
            steer = 1.0
        if steer < -1.0:
            steer = -1.0

        return steer


class DriveControlSimulation(DriveControl):
    '''
    DriveControl class for simulation (AckermannMsgs)
    '''

    def __init__(self, max_speed=0.0):
        '''
        Constructor

        :param max_speed: Float max value for speed values
        '''

        super(DriveControlSimulation, self).__init__()
        self.max_speed = max_speed     # speed in m/s
        self.zero_speed = -1.0         # neutral speed value
        self.zero_steering_angle = 0.0 # neutral steering value

    def create_command(self, steer=0.0, speed=0.0):
        '''
        Creates a command msg with the given sterring and speef value
        scales the values to the set range

        :param steer: Float steer value
        :param speed: Float speed value

        :return AckermannMsg with the scaled steering and speed
        '''

        a_d_s = AckermannDriveStamped()
        a_d_s.drive.steering_angle = self._scale_steer(steer)  # from -1 to 1
        a_d_s.drive.steering_angle_velocity = 0.0
        a_d_s.drive.speed = self._scale_speed(speed)  # directly in m/s
        a_d_s.drive.acceleration = 0.0
        a_d_s.drive.jerk = 0.0

        return a_d_s

    def _scale_speed(self, speed=0.0):
        '''
        Scales the speed to the set range

        :param speed: Float speed value to scale

        :return Float scaled speed value
        '''

        if speed < 0:
            speed = (self.max_speed / 2) - ((self.max_speed / 2) * abs(speed))
        else:
            speed = (self.max_speed / 2) + ((self.max_speed / 2) * speed)

        return speed

    def stop_engine(self):
        '''
        Creates neutral AckermannMsg

        :return AckermannMsg with neutral steering and speed
        '''

        a_d_s = AckermannDriveStamped()
        a_d_s.drive.steering_angle = self.zero_steering_angle
        a_d_s.drive.steering_angle_velocity = 0.0
        a_d_s.drive.speed = self._scale_speed(self.zero_speed)
        a_d_s.drive.acceleration = 0.0
        a_d_s.drive.jerk = 0.0

        return a_d_s


class DriveControlRacer(DriveControl):
    '''
    DriveControl class for racecar (TwistMsg)
    '''

    def __init__(self, max_speed=0.0):
        '''
        Constructor

        :param max_speed: Float max value for speed values
        '''

        super(DriveControlRacer, self).__init__()
        self.max_speed = max_speed     # speed in m/s
        self.zero_speed = 0.0          # neutral speed value
        self.zero_steering_angle = 0.0 # neutral steering value

    def create_command(self, steer=0.0, speed=0.0):
        '''
        Creates command msg to control the racecar (TwistMsgs)

        :param steer: Float steer value of the msg
        :param speed: Float speed value of the msg

        :return TwistMsg with steer value in twist.angular.z
                         and speed in twist.linear.x
        '''

        twist = Twist()
        twist.angular.z = self._scale_steer(steer) # from -1 to 1
        twist.linear.x = self._scale_speed(speed)  # from -self.max_speed to self.max_speed

        return twist

    def _scale_speed(self, speed=0.0):
        '''
        Scales the speed to the set range (+- self.max_speed)

        :param speed: Float speed value to scale

        :return Float scaled speed value
        '''

        if speed < -self.max_speed:
            speed = -self.max_speed
        if speed > self.max_speed:
            speed = self.max_speed
        return speed

    def stop_engine(self):
        '''
        Creates neutral TwistMsg

        :return TwistMsg with neutral steering and speed
        '''

        twist = Twist()
        twist.angular.z = self.zero_steering_angle
        twist.linear.x = self.zero_speed

        return twist
