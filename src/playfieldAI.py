#!/usr/bin/env python
import argparse
import os
import sys
sys.path.append(os.path.dirname(
                    os.path.dirname(
                        os.path.dirname(os.path.abspath(__file__))
                    )
                ))
from neuroracer_utils.YamlLoader import *

from core.Autonomous import *
from core.BaseModel import *
from processors.ImageProcessor import *
from processors.VisualizationProcessor import *
from utils.file_writer import *

from keras.callbacks import ModelCheckpoint



def main():

    file_writer = FileWriter()

    width  = 128
    height = 72
    depth  = 3

    root_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    img_processor = ImageProcessor(img_width = width, img_height = height,
                                   crop_top  = 0,  crop_bottom   = 100,
                                   do_grayscale_image = False,
                                   do_resize_image    = True,
                                   do_crop_image      = False,
                                   do_normalize_image = True,
                                   do_transform_image_for_backend = True)

    output_shape = img_processor.get_output_shape()

    color_scale = 'mono8' if output_shape.depth == 1 else 'bgr8'

    nn_function = '_steering_htw_tanh'
    model_path = root_path + '/trained_data/test_{}x{}x{}_no_crop_steering_htw_tanh-tf.hdf5'.format( width, height, depth )

    model = RegressionModel(model_name  = 'ba_{}x{}x{}_no_crop{}'.format(width, height, depth, nn_function),
                            nn_function = nn_function,
                            color_scale = 'bgr8',
                            img_cols    = output_shape.width,
                            img_rows    = output_shape.height,
                            img_depth   = output_shape.depth,
                            model_path  = model_path,
                            verbose     = 0,
                            training    = True)

    ai = Autonomous(model         = model,
                    img_processor = img_processor,
                    vis_processor = None,
                    file_writer   = file_writer,
                    cam_topic     = '/camera/front/image_rect_color/compressed',
                    cam_img_type  = 'compr',
                    visualize     = False,
                    verbose       = 1,
                    record        = False)

    print ('Start ai ...')
    ai.start(max_speed=0.8, wait_for_input=False, is_simulation=False, queue_size=1)


if __name__ == '__main__':
    main()
