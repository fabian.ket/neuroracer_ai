#!/usr/bin/env python

from processors.ImageProcessor import *
import unittest
import numpy as np
import os, shutil
import time
import cv2

root_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

class TestImageProcessor(unittest.TestCase):

    def test_all_img_processings(self):
        img_processor = ImageProcessor(img_width = 256, img_height = 144,
                                       crop_top = 10, crop_bottom = 90,
                                       do_grayscale_image = True,
                                       do_resize_image    = True,
                                       do_crop_image      = True,
                                       do_normalize_image = True,
                                       do_transform_image_for_backend = True)

        images = (np.ones((10, 300, 400, 3)) * 255).astype('uint8')

        images = img_processor.execute_img_list_processing(images)

        self.assertIsInstance( images, np.ndarray)
        self.assertTupleEqual( images.shape, (10, 115, 256, 1))

        toleranz = 0.00001
        norm_value = np.divide(255, 255.0)

        self.assertTrue(images[0][0][0][0] >= (norm_value - toleranz ) )
        self.assertTrue(images[0][0][0][0] <= (norm_value + toleranz ) )

    def test_grayscaling_grayscaled_images(self):
        img_processor = ImageProcessor()

        images = (np.ones((10, 300, 400, 1)) * 128).astype('uint8')

        images = img_processor.grayscale_img_list(images)

        self.assertIsInstance( images, np.ndarray)
        self.assertTupleEqual( images.shape, (10, 300, 400, 1))

    def test_get_output_shape(self):
        img_processor = ImageProcessor(img_width = 256, img_height = 144,
                                       crop_top = 0, crop_bottom = 100)

        output_shape = img_processor.get_output_shape()

        self.assertTupleEqual( output_shape, (256, 144, 3) )

    def test_denormalize_image(self):
        img_processor = ImageProcessor()

        images = (np.ones((10, 300, 400, 1)) * 255).astype('uint8')
        images = img_processor.normalize_img_list(images)
        images = img_processor.denormalize_img_list(images)

        toleranz = 0.00001
        norm_value = 255.0

        self.assertTrue(images[0][0][0][0] >= (norm_value - toleranz ) )
        self.assertTrue(images[0][0][0][0] <= (norm_value + toleranz ) )

    def test_get_cropped_height(self):
        img_processor = ImageProcessor()

        height = img_processor.get_cropped_height()

        self.assertTrue(height == 270)

        img_processor = ImageProcessor(img_width = 480, img_height  = 270,
                                       crop_top  = 10,  crop_bottom = 90,
                                       do_crop_image = True)

        height = img_processor.get_cropped_height()

        self.assertTrue( height == 216 )

if __name__ == '__main__':
    unittest.main()
