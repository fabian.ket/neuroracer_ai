#!/usr/bin/env python
import argparse
import os
import sys
sys.path.append(os.path.dirname(
                    os.path.dirname(
                        os.path.dirname(os.path.abspath(__file__))
                    )
                ))
from neuroracer_utils.YamlLoader import *

from core.Trainer import *
from core.BaseModel import *
from processors.ImageProcessor import *
from processors.VisualizationProcessor import *
from utils.Converter import *
from utils.file_writer import *

from keras.callbacks import ModelCheckpoint



def main():

    file_writer = FileWriter()

    width  = 256
    height = 144
    depth  = 3

    root_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    img_processor = ImageProcessor(img_width = width, img_height = height,
                                   crop_top  = 0,  crop_bottom   = 100,
                                   do_grayscale_image = False,
                                   do_resize_image    = True,
                                   do_crop_image      = False,
                                   do_normalize_image = True,
                                   do_transform_image_for_backend = True)

    output_shape = img_processor.get_output_shape()

    converter = Converter(root_path, img_processor=img_processor, img_type='compr', simulation=False)

    if False:
        converter.convert_rosbags_to_data()
        exit()

    print ('Start converting ...')
    (x_train, y_train), (x_test, y_test) = converter.return_rosbags(bags=['train/538s_counterclockwise_light_on'])
    print ('Converting finished')

    if True:
        (x_test, y_test) = (x_train, y_train)

    color_scale = 'mono8' if output_shape.depth == 1 else 'bgr8'

    nn_function = '_steering_alex'
    model_path = None # root_path + '/trained_data/ba_128x72x3_no_crop_steering_htw_tanh-tf.hdf5'

    model = RegressionModel(model_name  = 'ba_{}x{}x{}_no_crop{}'.format(width, height, depth, nn_function),
                            nn_function = nn_function,
                            color_scale = 'bgr8',
                            img_cols    = output_shape.width,
                            img_rows    = output_shape.height,
                            img_depth   = output_shape.depth,
                            model_path  = model_path,
                            verbose     = 0,
                            training    = True)

    # TODO: TEST data_augmentation

    trainer = Trainer(model = model,
                      x_train = x_train, y_train = y_train,
                      x_test  = x_test,  y_test  = y_test,
                      batch_size = 128,  epochs  = 2,
                      do_data_augmentation = False,
                      log_nn_structure     = True,
                      log_performance      = True,
                      log_tensor_board     = True,
                      file_writer = file_writer,
                      verbose     = 1)

    print ('Start training ...')
    trainer.start()


if __name__ == '__main__':
    main()
