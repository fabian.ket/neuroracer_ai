#!/usr/bin/env python

from utils.Converter import *
from processors.ImageProcessor import *
from core.BaseModel import *
import unittest
import numpy as np
import os, shutil
import time
from keras.engine.training import Model
from keras.wrappers.scikit_learn import KerasRegressor

root_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

class TestBaseModel(unittest.TestCase):

    def test_prediction(self):
        img_processor = ImageProcessor(img_width = 128, img_height = 72,
                                       crop_top = 10, crop_bottom = 90,
                                       do_grayscale_image = True,
                                       do_resize_image    = True,
                                       do_crop_image      = True,
                                       do_normalize_image = True,
                                       do_transform_image_for_backend = True)

        conv = Converter(root_path, img_processor=img_processor, simulation=False)

        bag_list= ['train/left_right_bag_for_unittest',
                   'test/left_right_bag_for_unittest']

        (x_train, y_train), (x_test, y_test) = conv.return_rosbags(bags=bag_list)

        output_shape = img_processor.get_output_shape()
        color_scale = 'mono8' if output_shape.depth == 1 else 'bgr8'

        model_path = root_path + '/trained_data/steering_default_mono8-tf-128x72-crop_t10_b90_unittest.hdf5'

        model = RegressionModel(model_name  = 'unittest_model',
                                nn_function = '_steering_htw',
                                color_scale = color_scale,
                                img_cols    = output_shape.width,
                                img_rows    = output_shape.height,
                                img_depth   = output_shape.depth,
                                model_path  = model_path,
                                verbose     = 1,
                                training    = False)

        prediction = model.predict_on_batch(x_train[0])

        self.assertIsInstance(prediction, np.ndarray)
        self.assertTupleEqual(prediction.shape, (1, 2))

    def test_false_input_shaped_image(self):
        model_path = root_path + '/trained_data/steering_default_mono8-tf-128x72-crop_t10_b90_unittest.hdf5'

        model = RegressionModel(model_name  = 'unittest_model',
                                nn_function = '_steering_htw',
                                color_scale = 'mono8',
                                img_cols    = 128,
                                img_rows    = 57,
                                img_depth   = 1,
                                model_path  = model_path,
                                verbose     = 1,
                                training    = False)

        image = np.ones((72, 128, 1)).astype('uint8')

        with self.assertRaises(ValueError):
            prediction = model.predict_on_batch(image)

    def test_false_model_path(self):
        model_path = root_path + ''

        with self.assertRaises(IOError):
            model = RegressionModel(model_name  = 'unittest_model',
                                    nn_function = '_steering_htw',
                                    color_scale = 'mono8',
                                    img_cols    = 128,
                                    img_rows    = 57,
                                    img_depth   = 1,
                                    model_path  = model_path,
                                    verbose     = 1,
                                    training    = False)

    def test_load_model(self):
        model_path = root_path + '/trained_data/steering_default_mono8-tf-128x72-crop_t10_b90_unittest.hdf5'

        model = RegressionModel(model_name  = 'unittest_model',
                                nn_function = '_steering_htw',
                                color_scale = 'mono8',
                                img_cols    = 128,
                                img_rows    = 57,
                                img_depth   = 1,
                                model_path  = model_path,
                                verbose     = 1,
                                training    = False)

        model.load_model(path=model_path)

        self.assertIsInstance(model.model, Model)

    def test_defaul_model_file_not_found(self):
        with self.assertRaises(IOError):
            model = RegressionModel(model_name  = 'unittest_model',
                                    nn_function = '_steering_htw',
                                    color_scale = 'mono8',
                                    img_cols    = 128,
                                    img_rows    = 57,
                                    img_depth   = 1,
                                    model_path  = None,
                                    verbose     = 1,
                                    training    = False)

    def test_training_model_is_keras_regressor(self):
        model = RegressionModel(model_name  = 'unittest_model',
                                nn_function = '_steering_htw',
                                color_scale = 'mono8',
                                img_cols    = 128,
                                img_rows    = 57,
                                img_depth   = 1,
                                model_path  = None,
                                verbose     = 1,
                                training    = True)

        self.assertIsInstance(model.model, KerasRegressor)


if __name__ == '__main__':
    unittest.main()






    
