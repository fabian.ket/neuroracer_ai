#!/usr/bin/env python
import argparse
import os
import sys
sys.path.append(os.path.dirname(
                    os.path.dirname(
                        os.path.dirname(os.path.abspath(__file__))
                    )
                ))
from neuroracer_utils.YamlLoader import *

from core.Trainer import *
from core.BaseModel import *
from processors.ImageProcessor import *
from processors.VisualizationProcessor import *
from utils.Converter import *
from utils.file_writer import *

from keras.callbacks import ModelCheckpoint



def main():

    # argument parser for commandline
    args, leftovers = _init_arg_parser()

    root_dir = os.path.dirname(os.path.abspath(__file__))

    # Set config file paths
    model_cfg_path          = root_dir + '/configs/model/default_mono8.yaml'
    processing_cfg_path     = root_dir + '/configs/processing/default.yaml'
    img_processing_cfg_path = root_dir + '/configs/processing/img_processing/img_processor_mono8.yaml'
    autonomous_cfg_path      = root_dir + '/configs/run/autonomous/default.yaml'
    training_cfg_path       = root_dir + '/configs/run/training/default.yaml'

    # Load yaml config files
    model_cfg          = load_yaml(model_cfg_path)
    processing_cfg     = load_yaml(processing_cfg_path)
    img_processing_cfg = load_yaml(img_processing_cfg_path)
    autonomous_cfg      = load_yaml(autonomous_cfg_path)
    training_cfg       = load_yaml(training_cfg_path)

    # Check if all files are loaded correctly
    if model_cfg is None:
        raise IOError('No such file: {}'.format( model_cfg_path ))

    if processing_cfg is None:
        raise IOError('No such file: {}'.format( processing_cfg_path ))

    if img_processing_cfg is None:
        raise IOError('No such file: {}'.format( img_processing_cfg_path ))

    if autonomous_cfg is None:
        raise IOError('No such file: {}'.format( autonomous_cfg_path ))

    if training_cfg is None:
        raise IOError('No such file: {}'.format( training_cfg_path ))


    # set flat for path, whether it is simulation or real world
    simulation = training_cfg['data']['simulation']

    convert_from_bags = training_cfg['data']['convert_from_bags']
    load_from_data    = training_cfg['data']['load_from_data']

    data_sub_folders      = training_cfg['data']['data_sub_folders']  # empty list will use all subfolders
    data_bags             = training_cfg['data']['data_bags']  # empty list will use all bags
    bags_to_convert       = training_cfg['data']['bags_to_convert']  # empty list will use all bags
    stop_after_converting = training_cfg['data']['stop_after_converting']  # target directory inside training/data/
    converter_path        = training_cfg['data']['converter_path']
    img_type              = training_cfg['data']['img_type']

    data_augmentation = training_cfg['run']['data_augmentation']
    batch_size        = training_cfg['run']['batch_size']
    epochs            = training_cfg['run']['epochs']

    # set debug info
    if args.verbose is not None:
        verbose = args.verbose
    else:
        verbose = training_cfg['log']['verbose']

    log_performance  = training_cfg['log']['log_performance']
    log_nn_structure = training_cfg['log']['log_nn_structure']
    log_tensor_board = training_cfg['log']['log_tensor_board']  # log file can become quite large

    # ImageProcessor config
    img_width          = img_processing_cfg['img_width']
    img_height         = img_processing_cfg['img_height']
    crop_top           = img_processing_cfg['crop_top']
    crop_bottom        = img_processing_cfg['crop_bottom']
    do_grayscale_image = img_processing_cfg['do_grayscale_image']
    do_resize_image    = img_processing_cfg['do_resize_image']
    do_crop_image      = img_processing_cfg['do_crop_image']
    do_normalize_image = img_processing_cfg['do_normalize_image']
    do_transform_image_for_backend = img_processing_cfg['do_transform_image_for_backend']

    # BaseModel config
    model_name  = model_cfg['model_name']
    nn_function = model_cfg['nn_function']
    model_path = model_cfg['model_path']

    file_writer = FileWriter()

    img_processor = ImageProcessor(img_width = img_width, img_height = img_height,
                                   crop_top  = crop_top, crop_bottom = crop_bottom,
                                   do_grayscale_image = do_grayscale_image,
                                   do_resize_image    = do_resize_image,
                                   do_crop_image      = do_crop_image,
                                   do_normalize_image = do_normalize_image,
                                   do_transform_image_for_backend = do_transform_image_for_backend)

    output_shape = img_processor.get_output_shape()

    converter = Converter(converter_path, img_processor=img_processor, img_type=img_type, simulation=simulation)

    if convert_from_bags:
        print('Converting bags...')
        # Convert from rosbags to data
        converter.convert_rosbags_to_data(bags=bags_to_convert)
        if stop_after_converting:
            exit()

    if load_from_data:
        # Get data from data folder
        (x_train, y_train), (x_test, y_test) = converter.return_data(dirs=data_sub_folders)
    else:
        # Get data from rosbags directly
        (x_train, y_train), (x_test, y_test) = converter.return_rosbags(bags=data_bags)

    color_scale = 'mono8' if output_shape.depth == 1 else 'bgr8'

    model = RegressionModel(model_name  = model_name,
                            nn_function = nn_function,
                            color_scale = color_scale,
                            img_cols    = output_shape.width,
                            img_rows    = output_shape.height,
                            img_depth   = output_shape.depth,
                            model_path  = model_path,
                            verbose     = verbose,
                            training    = True)

    # TODO: TEST data_augmentation

    trainer = Trainer(model = model,
                      x_train = x_train, y_train = y_train,
                      x_test  = x_test,  y_test  = y_test,
                      batch_size = batch_size,  epochs = epochs,
                      do_data_augmentation = data_augmentation,
                      log_nn_structure     = log_nn_structure,
                      log_performance      = log_performance,
                      log_tensor_board     = log_tensor_board,
                      file_writer = file_writer,
                      verbose     = verbose)

    trainer.start()

    # vis_processor = VisualizationProcessor(model=model.model, visualize=True)

    # file_writer = FileWriter()
    #
    # record = False

    # for image in x_train:
    #     corresponding_img_name = None
    #     image *= 255.
    #     if record:
    #         corresponding_img_name = file_writer.write_image(image, 'vis/raw', corresponding_img_name)

        # if vis_processor.visualize:
            # vis_processor.visualize_saliency(image, corresponding_img_name, 'prediction')


def _init_arg_parser():
    # argument parser for commandline
    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--run", type=str, help="set run config by name")
    parser.add_argument("-m", "--model", type=str, help="set model config by name")
    parser.add_argument("-ep", "--processing", type=str, help="set general processing config by name")
    parser.add_argument("-ip", "--image-processing", type=str, help="set image processing config by name")
    parser.add_argument("-v", "--verbose", type=int, choices=[0, 1, 2], help="set output verbosity")
    return parser.parse_known_args()


if __name__ == '__main__':
    main()
