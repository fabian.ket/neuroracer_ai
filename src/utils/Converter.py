#!/usr/bin/env python
from __future__ import print_function

import sys
import os
from cv2 import imread, imwrite, imshow, waitKey, cvtColor, COLOR_BGR2GRAY, destroyAllWindows
from multiprocessing import cpu_count
from posixpath import normpath

import numpy as np
import yaml
from pymp import Parallel, shared
from rosbag import Bag
from rospy import loginfo
from rospy.rostime import Time

sys.path.append(os.path.dirname(
                    os.path.dirname(
                        os.path.dirname(
                            os.path.dirname(os.path.abspath(__file__))
                        )
                    )
                ))

from neuroracer_utils.CvBridgeHandler import *
from utils.CommandMsgHandler import *

# directory prefixes for bag and data handling
PREFIX_TEST = 'test'
PREFIX_TRAIN = 'train'

REDUCE_INPUT_BY_SECOND = 2  # to avoid corrupted/not synchronized data at start/end

# ROS topics for simulation
TOPIC_CAMERA_SIM = "/camera/zed/rgb/image_rect_color/compressed"
TOPIC_COMMANDS_SIM = '/vesc/ackermann_cmd_mux/output'

# ROS topics for Neuroracer
TOPIC_CAMERA_REAL = "/camera/front/image_rect_color/compressed"
TOPIC_COMMANDS_REAL = '/neuroracer_teleop_joy/cmd_vel'

MAX_NB_CPU = 8 # limit maximum number of processes to use

class Converter:
    '''
    Converts rosbags or images + yaml file to np.array of images and y values
    '''

    def __init__(self, path, img_processor=None, img_type='compr', simulation=True):
        '''
        Constructor

        :param path:          String to directory which contains the <training_data> folder
        :param img_processor: ImageProcessor used to preprocess the images while converting
                              if None, the raw images will be loaded in the memory
        :param img_type:      String to decide if compr or raw images are in the rosbag files
        :param simulation:    Bool to decide if Ackermann or Twist msgs are in the rosbag files
                              if True, AckermannMsgs will be used

        :raise IOError if path is not a directory
        '''

        if not os.path.isdir(path):
            raise IOError('No such dir: {}'.format( path ))

        self.path = path

        self.img_processor = img_processor

        self.img_type = img_type

        self.simulation = simulation

        # handler for the diffenert image msgs types
        self.cv_bridge_handler = CvBridgeHandlerRaw() if self.img_type == 'raw' else CvBridgeHandlerCompr()

        # limit amount of used processes
        self.nb_cpu = cpu_count() if cpu_count() < MAX_NB_CPU else MAX_NB_CPU

        # handler for command msgs
        self.command_msg_handler = CommandMsgHandlerAckermann() if simulation else CommandMsgHandlerTwist()

    def execute_img_processing(self, image):
        '''
        Executes image processing if self.img_processor is set

        :param image: np.array the image to process

        return np.array the processed image
        '''

        if self.img_processor is not None:
            image = self.img_processor.execute_img_processing(image)

        return image

    def _create_all_bags(self):
        '''
        Creats a list of rosbag files in self.path

        :return List<String> of paths of rosbag files
        '''

        tmp_dir = self.path
        # if no bags are defined, all available are used
        bags = []
        # test bag folder
        tmp_folder = os.path.join(tmp_dir, "training_data/rosbags/test")
        tmp_folder = normpath(tmp_folder)
        for tmp_file in os.listdir(tmp_folder):
            if tmp_file.endswith(".bag"):
                tmp_bag = os.path.join(tmp_folder, tmp_file)
                tmp_bag = normpath(tmp_bag)
                bags.append(tmp_bag)

        # train bag folder
        tmp_folder = os.path.join(tmp_dir, "training_data/rosbags/train")
        tmp_folder = normpath(tmp_folder)
        for tmp_file in os.listdir(tmp_folder):
            if tmp_file.endswith(".bag"):
                tmp_bag = os.path.join(tmp_folder, tmp_file)
                tmp_bag = normpath(tmp_bag)
                bags.append(tmp_bag)

        return bags

    def _create_bags_with_bag_list(self, bags):
        '''
        Creats a list of rosbag files based on bags
        the rosbag files in bags will be used

        :param bags: List<String> list of rosbag filenames

        :return List<String> of paths of rosbag files

        :raise IOError if rosbag file in bags can not be found
        '''

        tmp_dir = self.path
        # use defined bags
        # bags should be defined as a list consisting of prefix-path (test/train) and bag name
        # example:
        # bags = ('test/test_bag_1', 'train/train_bag_1')
        tmp_bags = []
        for prefix in bags:
            tmp_bag = "training_data/rosbags/%s.bag" % (prefix,)
            tmp_bag = os.path.join(tmp_dir, tmp_bag)
            tmp_bag = normpath(tmp_bag)
            if os.path.isfile(tmp_bag):
                tmp_bags.append(tmp_bag)
            else:
                raise IOError('No such file: %s' % (tmp_bag,))
        return tmp_bags



    def convert_rosbags_to_data(self, bags=None):
        """
        Converts rosbag files to images and yaml files

        :param bags: List<String> use list of bags, empty list uses whole folder rosbags

        :raise IOError if bags is not a list
        """
        # root file path, use real path since folder is out of /src scope
        tmp_dir = self.path

        if not bags:
            # if no bags are defined, all available are used
            bags = self._create_all_bags()

        elif isinstance(bags, list):
            bags = self._create_bags_with_bag_list(bags)

        else:
            raise IOError('Passed variable is not an instance of type list')

        # NOTE:
        # rosbags are not thread safe (http://wiki.ros.org/rosbag/Code%20API), otherwise openmp loops could be used
        # inside each bag.
        # process bags in parallel
        with Parallel(self.nb_cpu) as p:
            for bag in p.iterate(bags):
                # extract bag name
                tmp = bag.split("/")
                bag_name = str(tmp[-1].replace(".bag", ""))
                prefix = "%s/%s" % (tmp[-2], bag_name)

                # build file path for folder containing data and create if not exist
                tmp_folder_dir = "training_data/data/%s" % (prefix,)
                tmp_folder_dir = os.path.join(tmp_dir, tmp_folder_dir)
                tmp_folder_dir = normpath(tmp_folder_dir)
                if not os.path.exists(tmp_folder_dir):
                    os.makedirs(tmp_folder_dir)

                # set bag's path for data
                if self.simulation:
                    topic_camera = TOPIC_CAMERA_SIM
                    topic_commands = TOPIC_COMMANDS_SIM
                else:
                    topic_camera = TOPIC_CAMERA_REAL
                    topic_commands = TOPIC_COMMANDS_REAL

                # process bag
                data = {}
                with Bag(bag, 'r') as opened_bag:
                    # cut the first and last seconds, since sometimes
                    # the amount of messages from each type is not equal
                    start_time = Time(int(opened_bag.get_start_time() + REDUCE_INPUT_BY_SECOND))
                    end_time = Time(int(opened_bag.get_end_time() - REDUCE_INPUT_BY_SECOND))

                    # iterate over bag and extract data
                    for topic, msg, t in opened_bag.read_messages(start_time=start_time, end_time=end_time):
                        if topic == topic_camera:
                            for topic_2, msg_2, t_2 in opened_bag.read_messages(topics=topic_commands, start_time=t):
                                timestamp = "%s_%s" % (bag_name, t)
                                data[timestamp] = self.command_msg_handler.get_msg_data(msg_2)

                                cv_image = self.cv_bridge_handler.get_image(msg)
                                filename = "%s/%s_%s.jpg" % (tmp_folder_dir, bag_name, t)
                                filename = normpath(filename)
                                imwrite(filename, cv_image)
                                # break after first data is received
                                break

                # build file path and save data
                filename = "%s/%s.yaml" % (tmp_folder_dir, bag_name)
                filename = normpath(filename)
                with open(filename, 'w') as outfile:
                    yaml.dump(data, outfile)

        loginfo('Converting successful.')

    def return_rosbags(self, bags=None, count=None):
        """
        Converts rosbag files to np.arrays

        :param bags: List<String> use list of bags, empty list uses whole folder rosbags
        :param count: Int number of data to return, if None all

        :return (np.array<np.array>, np.array<np.array>) (np.array<np.array>, np.array<np.array>)
                lists of images and y values

        :raise IOError if bags is not a list
        :raise IOError if x and y lists are not from the same size

        BUG: if the shared.list() return values are changed after this method was called,
             PYMP gets stuck on the second call on this method
             do not know why, if only one thread is used, it doesn't get stuck

             PYMP doesn't get stuck if the return value is changed with a method
             which also uses PYMP shared.list()s
        """
        # root file path, use real path since folder is out of /src scope
        tmp_dir = self.path

        # handle bag input
        if not bags:
            # if no bags are defined, all available are used
            bags = self._create_all_bags()

        elif isinstance(bags, list):
            bags = self._create_bags_with_bag_list(bags)

        else:
            raise IOError('Passed variable is not an instance of type list')

        # allocate and initialize shared memory for data
        x_train = shared.list()
        y_train = shared.list()
        x_test = shared.list()
        y_test = shared.list()

        # set bag's path for data
        if self.simulation:
            topic_camera = TOPIC_CAMERA_SIM
            topic_commands = TOPIC_COMMANDS_SIM
        else:
            topic_camera = TOPIC_CAMERA_REAL
            topic_commands = TOPIC_COMMANDS_REAL

        with Parallel(self.nb_cpu) as p:
            for bag in p.iterate(bags):

                # stop if count is reached
                if count is not None:
                    if ( len(x_train) + len(x_test) ) >= count:
                        break

                # extract folder prefix
                tmp = bag.split("/")
                prefix = str(tmp[-2])  # folder test/train
                with Bag(bag, 'r') as opened_bag:
                    # cut the first and last seconds, since sometimes the messages from each topic are not synchronized
                    start_time = Time(int(opened_bag.get_start_time() + REDUCE_INPUT_BY_SECOND))
                    end_time = Time(int(opened_bag.get_end_time() - REDUCE_INPUT_BY_SECOND))

                    # iterate for 'train' and 'test' data

                    for topic, msg, t in opened_bag.read_messages(topics=topic_camera, start_time=start_time, end_time=end_time):

                        # stop if count is reached
                        if count is not None:
                            if ( len(x_train) + len(x_test) ) >= count:
                                break

                        for topic_2, msg_2, t_2 in opened_bag.read_messages(topics=topic_commands,
                                                                            start_time=t):
                            # add matching pair
                            image_data = self.cv_bridge_handler.get_image(msg)

                            image_data = np.array(image_data)

                            image_data = self.execute_img_processing(image_data)

                            command_msg_data = self.command_msg_handler.get_msg_data(msg_2)
                            steering_angle = command_msg_data['steering_angle']
                            speed = command_msg_data['speed']

                            with p.lock:
                                if prefix == PREFIX_TEST:
                                    x_test.append(image_data)
                                    y_test.append((steering_angle, speed))
                                if prefix == PREFIX_TRAIN:
                                    x_train.append(image_data)
                                    y_train.append((steering_angle, speed))
                            # break after first data is received
                            break

        x_train = np.asarray(x_train)
        y_train = np.asarray(y_train)
        x_test = np.asarray(x_test)
        y_test = np.asarray(y_test)

        if x_train.shape[0] != y_train.shape[0]:
            raise IOError(
                'Error: x_train and y_train have different shapes: %s | %s' % (x_train.shape[0], y_train.shape[0]))

        if x_test.shape[0] != y_test.shape[0]:
            raise IOError(
                'Error: x_test and y_test have different shapes: %s | %s' % (x_test.shape[0], y_test.shape[0]))

        return (x_train, y_train), (x_test, y_test)


    def _create_all_imgs_list(self):
        '''
        Creats a list of directories in self.path

        :return List<String> of path of dirs
        '''

        tmp_dir = self.path

        dirs = []
        # test data folder
        tmp_folder_test = os.path.join(tmp_dir, "training_data/data/test")
        tmp_folder_test = normpath(tmp_folder_test)
        for tmp_file in os.listdir(tmp_folder_test):
            tmp_folder = os.path.join(tmp_folder_test, tmp_file)
            tmp_folder = normpath(tmp_folder)
            if os.path.isdir(tmp_folder):
                dirs.append(tmp_folder)
        # train data folder
        tmp_folder_train = os.path.join(tmp_dir, "training_data/data/train")
        tmp_folder_train = normpath(tmp_folder_train)
        for tmp_file in os.listdir(tmp_folder_train):
            tmp_folder = os.path.join(tmp_folder_train, tmp_file)
            tmp_folder = normpath(tmp_folder)
            if os.path.isdir(tmp_folder):
                dirs.append(tmp_folder)

        return dirs

    def _create_imgs_with_dir_list(self, dirs):
        '''
        Creats a list of directories based on dirs
        the directories dirs will be used

        :param dirs: List<String> list of directory names

        :return List<String> of paths of directories

        :raise IOError if directory in dirs can not be found
        '''

        tmp_dir = self.path

        # use defined dirs
        # dirs should be defined as a list consisting of prefix-path (test/train) and dir name
        # example:
        # dirs = ('test/test_dir_1', 'train/train_dir_1')
        tmp_dirs = []
        for dir_name in dirs:
            tmp_folder = "training_data/data/%s" % (dir_name,)
            tmp_folder = os.path.join(tmp_dir, tmp_folder)
            tmp_folder = normpath(tmp_folder)
            if os.path.isdir(tmp_folder):
                tmp_dirs.append(tmp_folder)
            else:
                raise IOError('No such file: %s' % (tmp_folder,))

        return tmp_dirs

    def return_data(self, dirs=None, count=None):
        """
        Converts images and yaml files to np.arrays

        :param dirs: List<String> use list of folders, empty list uses whole folder training_data/data
        :param count: Int number of data to return, if None all

        :return (np.array<np.array>, np.array<np.array>) (np.array<np.array>, np.array<np.array>)
                lists of images and y values

        :raise IOError if dirs is not a list
        :raise IOError if x and y lists are not from the same size

        BUG: if the shared.list() return values are changed after this method was called,
             PYMP gets stuck on the second call on this method
             do not know why, if only one thread is used, it doesn't get stuck

             PYMP doesn't get stuck if the return values are changed with a method
             which also uses PYMP shared.list()s
        """
        # root file path, use real path since folder is out of /src scope
        tmp_dir = self.path

        if not dirs:
            # if no bags are defined, all available are used
            dirs = self._create_all_imgs_list()

        elif isinstance(dirs, list):
            dirs = self._create_imgs_with_dir_list(dirs)

        else:
            raise IOError('Passed variable is not an instance of type list')

        # allocate and initialize shared memory for data
        x_train = shared.list()
        y_train = shared.list()
        x_test = shared.list()
        y_test = shared.list()

        for t_dir in dirs:

            # stop if count is reached
            if count is not None:
                if ( len(x_train) + len(x_test) ) >= count:
                    break

            # extract folder prefix
            tmp = t_dir.split("/")
            dir_name = str(tmp[-1])
            prefix = str(tmp[-2])  # folder test/train
            # build path to data and retrieve it
            tmp = "%s/%s.yaml" % (t_dir, dir_name)
            tmp = normpath(tmp)
            with open(tmp) as data_file:
                data = yaml.load(data_file)

            # start parallel processing
            with Parallel(self.nb_cpu) as p:
                # iterate over the given 'y_train' data
                for key in p.iterate(data):

                    # stop if count is reached
                    if count is not None:
                        if ( len(x_train) + len(x_test) ) >= count:
                            break

                    # build corresponding image file path
                    image_name = "%s/%s.jpg" % (t_dir, key)
                    image_name = normpath(image_name)

                    # skip if image file path does not exist
                    if not os.path.exists(image_name):
                        continue

                    # read and process image data if enabled
                    cv_image = imread(image_name)

                    image_data = np.array(cv_image)

                    image_data = self.execute_img_processing(image_data)

                    # read according steering command
                    steering_angle = data[key]['steering_angle']
                    speed = data[key]['speed']

                    # save data to shared memory
                    with p.lock:
                        if prefix == PREFIX_TEST:
                            x_test.append(image_data)
                            y_test.append((steering_angle, speed))
                        elif prefix == PREFIX_TRAIN:
                            x_train.append(image_data)
                            y_train.append((steering_angle, speed))

        x_train = np.asarray(x_train)
        y_train = np.asarray(y_train)
        x_test = np.asarray(x_test)
        y_test = np.asarray(y_test)

        if x_train.shape[0] != y_train.shape[0]:
            raise IOError(
                'Error: x_train and y_train have different shapes: %s | %s' % (x_train.shape[0], y_train.shape[0]))

        if x_test.shape[0] != y_test.shape[0]:
            raise IOError(
                'Error: x_test and y_test have different shapes: %s | %s' % (x_test.shape[0], y_test.shape[0]))

        return (x_train, y_train), (x_test, y_test)
