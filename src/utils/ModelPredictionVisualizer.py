#!/usr/bin/env python

import cv2
from keras import backend as k
import numpy as np
import os
import time

MIN_STEER_VALUE = -1 # min vales for steering
MAX_STEER_VALUE =  1 # max value for steering

MIN_CIRCLE_RAD = 0.25 * np.pi # min radians for line drawings
MAX_CIRCLE_RAD = 0.75 * np.pi # max radinas for line drawings

class ModelPredictionVisualizer:
    '''
    Class to visualize the y value and the model prediction for the given x_data
    Model predictions are drawn with a blue line at the bottom middle,
    y values are drawn with a red line at the bottom middle,
    quadrant is from 45 degrees to 135 degrees

    :param model:  BaseModel pretrained model for the predictions
    :param x_data: np.array image data for the predictions
    :param y_data: np.array real steering values
    :param img_processor: ImageProcessor if None, x_data has to be preprocessed for the model
                                         else ModelPredictionVisualizer uses this img_processor
                                         to do the preprocessing by itself
    :param path: String path to the dir to save the images at
    '''

    def __init__(self, model, x_data, y_data, img_processor=None, path=None):
        self.model = model
        self.img_processor = img_processor
        self.x_data = x_data
        self.y_data = y_data
        self.path = path

    @staticmethod
    def _fmap (x, in_min, in_max, out_min, out_max):
        '''
        Maps the x value in range in_min to in_max to the range of out_min to out_max

        :param x:       Float value which should be mapped
        :param in_min:  Float min range value of x
        :param in_max:  Float max range value of x
        :param out_min: Float min range value to which x should mapped to
        :param out_max: Float max range value to which x should mapped to

        :return Float mapped x value
        '''

        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min

    def get_circle_point(self, steer):
        '''
        Calculates the circle point for the given steering value

        :param steer: Float the steering value

        :return (Float, Float) the circle point for the given steering value
                               as a tuple (X, Y)
        '''

        phi = self._fmap(steer, MIN_STEER_VALUE, MAX_STEER_VALUE, MIN_CIRCLE_RAD, MAX_CIRCLE_RAD)
        return (np.cos(phi), np.sin(phi))

    def visualize_prediction(self, output_resize_factor=1.0, draw_legend=False):
        '''
        Visualized all self.x_data images with prediction and y_value lines and if choosen
        a legend

        :param output_resize_factor: Float resized the output image with this factor
        :param draw_legend:          Bool if True draws a legend in the top left corner
        '''

        if not os.path.isdir(self.path):
            raise IOError ('No such dir: {}'.format(self.path))

        if output_resize_factor <= 0.0:
            output_resize_factor = 1.0

        self.path = os.path.abspath(self.path)

        for x, y in zip(self.x_data, self.y_data):
            x = np.array(x)

            image = x # don't touch x, so if it is the raw image [not preprocessed]
                      # you can draw the lines on the original image and save that

            if self.img_processor is not None:
                image = self.img_processor.execute_img_processing(x)

            if k.image_dim_ordering() == 'tf':
                # Tensorflow-Backend
                with self.model.graph.as_default():
                    steer, speed = np.squeeze(self.model.predict_on_batch(image))
            else:
                # Theano-Backend:
                # steer, speed = np.squeeze(self.model.predict_on_batch(x_data))
                pass

            steer_y, speed_y = np.squeeze(y)

            P_pre = self.get_circle_point(steer)
            P_real = self.get_circle_point(steer_y)

            line_img = x

            # if img_processor is None, than the images are preprocessed
            # and we have to denormalize it before saving
            if self.img_processor is None:
                line_img *= 255.0

            if line_img.shape[2] is 1:
                line_img = cv2.cvtColor(x, cv2.COLOR_GRAY2RGB)

            if output_resize_factor is not 1.0:
                line_img = cv2.resize(src=line_img, dsize=(int(x.shape[1] * output_resize_factor), int(x.shape[0] * output_resize_factor)))

            P_bottom_middle = (int(line_img.shape[1] / 2), line_img.shape[0]) # start point for the lines
            line_scale_factor = int(line_img.shape[0] / 3) # line length
            thickness = int(line_img.shape[1] / 16)        # line thickness

            P_pre = P_bottom_middle - np.multiply(P_pre, line_scale_factor)
            P_pre = (int(P_pre[0]), int(P_pre[1]))

            P_real = P_bottom_middle - np.multiply(P_real, line_scale_factor)
            P_real = (int(P_real[0]), int(P_real[1]))

            line_img = cv2.line(line_img, P_bottom_middle, P_real, (0,0,255), thickness)
            line_img = cv2.line(line_img, P_bottom_middle, P_pre, (255,0,0), thickness)

            if draw_legend:
                cv2.putText(line_img, 'y value', (15, int(10 * output_resize_factor)), cv2.FONT_HERSHEY_PLAIN, output_resize_factor, (0,0,255), 2 )
                cv2.putText(line_img, 'prediction', (15, int(30 * output_resize_factor)), cv2.FONT_HERSHEY_PLAIN, output_resize_factor, (255,0,0), 2 )

            filename = self.path + '/' + str(time.time()) + '.jpg'

            cv2.imwrite(filename=filename, img=line_img)
