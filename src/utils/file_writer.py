#!/usr/bin/env python
from cv2 import imwrite
from os import makedirs, path
from posixpath import normpath
from threading import Lock
from time import time, localtime, strftime

import matplotlib.pyplot as plt
from keras.utils.vis_utils import plot_model

lock = Lock()


def write_image(image, folder_name, file_name=None):
    '''
    Writes the given image, in the given dir, in the given file<name>

    :param image:       np.array image matrix
    :param folder_name: String for the directory name
    :param file_name:   String the filename of the image file, if None timestamp is used

    :return the used filename for the image
    '''

    # log image
    # use real path since folder is out of /src scope
    tmp_dir = path.realpath(path.dirname(__file__))
    file_path = path.join(tmp_dir, "../..", str(folder_name))

    if not file_name:
        file_name = int(round(time() * 1000))
    else:
        file_name = file_name

    file_path = "%s/%s.png" % (file_path, str(file_name),)
    file_path = normpath(file_path)
    imwrite(file_path, image)

    return file_name


def write_log_graphs(model_name=None, history=None):
    '''
    Logs the training history (losses) as png

    :param model_name: String used as dirname
    :param history:    Map<String, Float> keras training history to get log data from
    '''

    # use real path since folder is out of /src scope
    tmp_dir = path.realpath(path.dirname(__file__))
    file_path = "logs/%s" % (model_name,)
    file_path = normpath(path.join(tmp_dir, "../..", file_path))
    if not path.exists(file_path):
        makedirs(file_path)

    # path for accuracy log
    tmp_time = strftime("%Y-%m-%d %H:%M:%S", localtime())

    # path for loss log
    loss = normpath("%s/%s_loss.png" % (file_path, tmp_time,))

    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    loss_title = "%s loss" % (model_name,)
    plt.title(loss_title)
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig(loss)


def write_log_text(name, log_text, mode):
    '''
    Logs log_text with timestmp under dir name, mode is part of the filename

    :param name:     String name of the dir
    :param log_text: String to log
    :param mode:     String part of the filename (mostly autonomous or train)
    '''

    # use real path since folder is out of /src scope
    tmp_dir = path.realpath(path.dirname(__file__))
    file_path = "logs/%s" % (name,)
    file_path = normpath(path.join(tmp_dir, "../..", file_path))

    lock.acquire()

    if not path.exists(file_path):
        makedirs(file_path)

    tmp_time = strftime("%Y-%m-%d", localtime())
    file_path = normpath("%s/%s_%s.log" % (file_path, tmp_time, mode))

    with open(file_path, "a") as tmp_file:
        tmp_file.write("{}\n".format(log_text))

    lock.release()


def write_log_training(name, history):
    '''
    Logs the training history in the dir <name>

    :param name:    String name of the log dir
    :param history: Map<String, Float> Keras training history
    '''

    # use real path since folder is out of /src scope
    tmp_dir = path.realpath(path.dirname(__file__))
    file_path = "logs/%s" % (name,)
    file_path = normpath(path.join(tmp_dir, "../..", file_path))
    if not path.exists(file_path):
        makedirs(file_path)
    tmp_time = strftime("%Y-%m-%d %H:%M:%S", localtime())
    file_path = normpath("%s/%s.log" % (file_path, tmp_time,))

    for nb_epoch in history.epoch:
        log_data = "epoch: %s | loss: %s - val_loss: %s" % (
            str(nb_epoch + 1),
            str(history.history['loss'][nb_epoch]),
            str(history.history['val_loss'][nb_epoch])
        )
        with open(file_path, "a") as log_text:
            log_text.write("{}\n".format(log_data))


def write_nn_structure(model=None):
    '''
    Logs the model nn structure as png, uses the model_name as
    name for the log dir

    :param model: BaseModel model to get the structure from
    '''

    # use real path since folder is out of /src scope
    tmp_dir = path.realpath(path.dirname(__file__))
    model.model = model.load_model()
    file_path = "graphs/%s" % (model.model_name,)
    file_path = normpath(path.join(tmp_dir, "../..", file_path))
    if not path.exists(file_path):
        makedirs(file_path)
    file_path = "%s/%s_nn_structure.png" % (file_path, model.model_name,)
    file_path = normpath(file_path)
    plot_model(model.model, to_file=file_path, show_shapes=True, show_layer_names=True)


class FileWriter:
    '''
    Wrapper class for the logging functions
    '''

    def __init__(self):
        # nothing to do here
        pass

    @staticmethod
    def write_image(image, folder_name, file_name=None):
        '''
        Writes the given image in the given dir with the given name

        :param image:       np.array image matrix to save
        :param folder_name: String dir name to save in
        :param file_name:   String to save image as, if None timestamp is used
        '''
        # Wrapper to avoid modifying python path
        write_image(image, folder_name, file_name=file_name)

    @staticmethod
    def write_log_text(name, log_text, mode):
        '''
        Writes log_text in dir <name> mode is used for filename

        :param name:     String dirname
        :param log_text: String text to log
        :param mode:     String part of the filename
        '''
        # Wrapper to avoid modifying python path
        write_log_text(name, log_text, mode)

    @staticmethod
    def write_nn_structure(model=None):
        '''
        Writes image of model nn structure

        :param model: BaseModel model to get structure from
        '''
        write_nn_structure(model=model)

    @staticmethod
    def write_log_graphs(model_name=None, history=None):
        '''
        Logs the loss history as png

        :param model_name: String name of the model
        :param history:    Map<String, Float> keras history of the training process
        '''

        write_log_graphs(model_name=model_name, history=history)

    @staticmethod
    def write_log_training(name, history):
        '''
        Logs the loss history as logfile

        :param name:    String dirname
        :param history: Map<String, Float> keras history of the training process
        '''

        write_log_training(name=name, history=history)
