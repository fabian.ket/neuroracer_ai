#!/usr/bin/env python

import abc

class CommandMsgHandler(object):
    '''
    Abstract base class for command msg handlers
    Task is to return data in command msgs as map
    '''

    __metaclass__ = abc.ABCMeta

    def __init__(self):
        '''
        Constructor
        '''

        pass

    @abc.abstractmethod
    def get_msg_data(self, msg):
        '''
        Return the data of commad msgs as map

        :return Map<String, Float> map indices are "steering_angle" and "speed"
        '''
        return

class CommandMsgHandlerTwist(CommandMsgHandler):
    '''
    Implemented command msg handler for twist msgs
    '''

    def get_msg_data(self, msg):
        '''
        Returns the data of a TwistMsg as Map<String, Float>

        :param msg: TwistMsg msg to get the data from

        :return Map<String, Float> the map with the steering and speed values
        '''

        return {
            'steering_angle': msg.angular.z,
            'speed': msg.linear.x
        }

class CommandMsgHandlerAckermann(CommandMsgHandler):
    '''
    Implemented command msg handler for ackermann msgs
    '''

    def get_msg_data(self, msg):
        '''
        Returns the data of a AckermannMsg as Map<String, Float>

        :param msg: AckermannMsg msg to get the data from

        :return Map<String, Float> the map with the steering and speed values
        '''

        return {
            'steering_angle': msg.drive.steering_angle,
            'speed': msg.drive.speed
        }
