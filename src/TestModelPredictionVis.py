#!/usr/bin/env python

from utils.Converter import *
from utils.ModelPredictionVisualizer import *
from processors.ImageProcessor import *
from core.BaseModel import *
import unittest
import numpy as np
import os, shutil
import time

root_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

class TestModelPredictionVisualizer(unittest.TestCase):

    def test_prediction_visualizion(self):
        img_processor = ImageProcessor(img_width = 128, img_height = 72,
                                       crop_top = 10, crop_bottom = 90,
                                       do_grayscale_image = True,
                                       do_resize_image    = True,
                                       do_crop_image      = True,
                                       do_normalize_image = True,
                                       do_transform_image_for_backend = True)

        conv = Converter(root_path, img_processor=None, simulation=False)

        bag_list= ['train/left_right_bag_for_unittest',
                   'test/left_right_bag_for_unittest']

        (x_train, y_train), (x_test, y_test) = conv.return_rosbags(bags=bag_list)

        output_shape = img_processor.get_output_shape()
        color_scale = 'mono8' if output_shape.depth == 1 else 'bgr8'

        model_path = root_path + '/trained_data/steering_default_mono8-tf-128x72-crop_t10_b90_unittest.hdf5'

        model = RegressionModel(model_name  = 'unittest_model',
                                nn_function = '_steering_htw',
                                color_scale = color_scale,
                                img_cols    = output_shape.width,
                                img_rows    = output_shape.height,
                                img_depth   = output_shape.depth,
                                model_path  = model_path,
                                verbose     = 1,
                                training    = False)

        model_pre_vis_path = root_path + '/modelPreVis{}'.format(time.time())
        os.makedirs(model_pre_vis_path)

        model_pre_vis = ModelPredictionVisualizer(model=model, x_data=x_train, y_data=y_train, img_processor=img_processor, path=model_pre_vis_path)
        model_pre_vis.visualize_prediction(output_resize_factor=2.5, draw_legend=True)


        model_pre_vis_file_list = os.listdir(model_pre_vis_path)

        self.assertTrue( len(model_pre_vis_file_list) == 702)

        try:
            shutil.rmtree(model_pre_vis_path)
        except Exception as e:
            print(e)

    def test_no_such_dir(self):
        model_pre_vis = ModelPredictionVisualizer(model=None, x_data=None, y_data=None, img_processor=None, path='')

        with self.assertRaises(IOError):
            model_pre_vis.visualize_prediction(output_resize_factor=2.5, draw_legend=True)

if __name__ == '__main__':
    unittest.main()
