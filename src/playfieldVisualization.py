#!/usr/bin/env python
import argparse
import os
import sys
sys.path.append(os.path.dirname(
                    os.path.dirname(
                        os.path.dirname(os.path.abspath(__file__))
                    )
                ))
from neuroracer_utils.YamlLoader import *

from core.Trainer import *
from core.BaseModel import *
from processors.ImageProcessor import *
from processors.VisualizationProcessor import *
from utils.ModelPredictionVisualizer import *
from utils.Converter import *
from utils.file_writer import *

from keras.callbacks import ModelCheckpoint



def main():

    file_writer = FileWriter()

    width  = 128
    height = 72
    depth  = 3

    root_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    img_processor = ImageProcessor(img_width = width, img_height = height,
                                   crop_top  = 0,  crop_bottom   = 100,
                                   do_grayscale_image = False,
                                   do_resize_image    = True,
                                   do_crop_image      = False,
                                   do_normalize_image = True,
                                   do_transform_image_for_backend = True)

    output_shape = img_processor.get_output_shape()

    converter = Converter(path='/home/ros', img_processor=None, img_type='compr', simulation=False)

    print ('Start converting ...')
    (x_train, y_train), (x_test, y_test) = converter.return_rosbags(bags=['test/test_225s_counterclockwise_light_on'], count=5000)
    print ('... converting finished')

    color_scale = 'mono8' if output_shape.depth == 1 else 'bgr8'

    nn_function = '_steering_htw_tanh'
    model_path = root_path + '/trained_data/test_128x72x3_no_crop_steering_htw_tanh-tf.hdf5'

    print ('Create model ...')
    model = RegressionModel(model_name  = 'ba_{}x{}x{}_no_crop{}'.format(width, height, depth, nn_function),
                            nn_function = nn_function,
                            color_scale = 'bgr8',
                            img_cols    = output_shape.width,
                            img_rows    = output_shape.height,
                            img_depth   = output_shape.depth,
                            model_path  = model_path,
                            verbose     = 1,
                            training    = False)

    modPreVis = ModelPredictionVisualizer(model=model,
                                          x_data=x_test, y_data=y_test,
                                          img_processor=img_processor,
                                          path='/home/ros/modPreVis')

    print ('Start visualization ...')
    modPreVis.visualize_prediction(draw_legend=True)
    print ('... visualization finished')

if __name__ == '__main__':
    main()
