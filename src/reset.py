#!/usr/bin/env python

import argparse

import rospy
from gazebo_msgs.msg import ModelState
from gazebo_msgs.srv import SetModelState
from geometry_msgs.msg import Pose
from tf.transformations import quaternion_from_euler

set_state = rospy.ServiceProxy("/gazebo/set_model_state", SetModelState)


def main():
    _reset_model_pos()


def _reset_model_pos():
    # DEFAULT PARAMS
    x = 4.003439
    y = 0.806448
    z = 0.358248

    roll = 0.0
    pitch = 0.0
    yaw = 0.0

    # argument parser for commandline
    args, leftovers = _init_arg_parser()

    # overwrite default configs if arguments are parsed
    if args.pos_x is not None:
        x = args.pos_x
    if args.pos_y is not None:
        y = args.pos_y
    if args.pos_z is not None:
        z = args.pos_z

    if args.roll is not None:
        roll = args.roll
    if args.pitch is not None:
        pitch = args.pitch
    if args.yaw is not None:
        yaw = args.yaw

    pose = Pose()
    pose.position.x = x
    pose.position.y = y
    pose.position.z = z

    q = quaternion_from_euler(roll, pitch, yaw)

    pose.orientation.x = q[0]
    pose.orientation.y = q[1]
    pose.orientation.z = q[2]
    pose.orientation.w = q[3]

    state = ModelState()
    state.model_name = 'racecar'
    state.pose = pose

    try:
        set_state(state)
    except rospy.ServiceException as e:
        raise rospy.ServiceException("Service call failed: %s" % (e,))


def _init_arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-px", "--pos-x", type=float, help="set x value of coordinate")
    parser.add_argument("-py", "--pos-y", type=float, help="set y value of coordinate")
    parser.add_argument("-pz", "--pos-z", type=float, help="set z value of coordinate")
    parser.add_argument("-r", "--roll", type=float, help="set roll")
    parser.add_argument("-p", "--pitch", type=float, help="set pitch")
    parser.add_argument("-y", "--yaw", type=float, help="set yaw")
    return parser.parse_known_args()


if __name__ == '__main__':
    main()
