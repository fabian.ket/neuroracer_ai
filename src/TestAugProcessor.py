#!/usr/bin/env python

from utils.Converter import *
from processors.ImageProcessor import *
from processors.AugmentationProcessor import *
import unittest
import numpy as np
import os, shutil
import time

root_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

class TestAugProcessor(unittest.TestCase):

    def test_create_aug_data(self):
        conv = Converter(root_path, img_processor=None, simulation=False)

        bag_list= ['train/left_right_bag_for_unittest']
        (x_train, y_train), (x_test, y_test) = conv.return_rosbags(bags=bag_list)

        aug_path = root_path
        dirname  = 'augmentation_for_unittest_{}'.format(time.time())
        prefix   = 'train'

        return_path = '{}/training_data/data/{}/{}'.format (aug_path, prefix, dirname)

        aug_processor = AugmentationProcessor(x_train, y_train)
        aug_processor.save_to_dir(aug_path, dirname=dirname, prefix=prefix, img_count=100)

        aug_list = os.listdir(return_path)

        self.assertTrue( len(aug_list) == 101 )

        try:
            shutil.rmtree(return_path)
        except Exception as e:
            print(e)

    def test_return_augmented_data(self):
        conv = Converter(root_path, img_processor=None, simulation=False)

        bag_list= ['train/left_right_bag_for_unittest']
        (x_train, y_train), (x_test, y_test) = conv.return_rosbags(bags=bag_list)

        aug_path = root_path
        dirname  = 'augmentation_for_unittest_{}'.format(time.time())
        prefix   = 'train'

        return_path = '{}/training_data/data/{}/{}'.format (aug_path, prefix, dirname)

        img_count = 150

        aug_processor = AugmentationProcessor(x_train, y_train)
        aug_processor.save_to_dir(aug_path, dirname=dirname, prefix=prefix, img_count=img_count)

        (x_train, y_train), (x_test, y_test) = conv.return_data(dirs=['{}/{}'.format(prefix, dirname)])

        self.assertIsInstance(x_train, np.ndarray)
        self.assertIsInstance(y_train, np.ndarray)
        self.assertTrue(x_train.shape[0] == img_count)
        self.assertTrue(y_train.shape[0] == img_count)

        try:
            shutil.rmtree(return_path)
        except Exception as e:
            print(e)

    def test_no_dir(self):
        aug_processor = AugmentationProcessor(None, None)
        with self.assertRaises(IOError):
            aug_processor.save_to_dir('', dirname='test', prefix='train', img_count=100)

if __name__ == '__main__':
    unittest.main()
