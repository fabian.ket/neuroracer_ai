#!/usr/bin/env python

from utils.Converter import *
from utils.file_writer import *
from processors.ImageProcessor import *
from core.BaseModel import *
from processors.VisualizationProcessor import *
import unittest
import numpy as np
import os, shutil
import time

root_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

class TestVisualizationProcessor(unittest.TestCase):

    def test_visualize_activation(self):
        img_processor = ImageProcessor(img_width = 128, img_height = 72,
                                       crop_top = 10, crop_bottom = 90,
                                       do_grayscale_image = True,
                                       do_resize_image    = True,
                                       do_crop_image      = True,
                                       do_normalize_image = True,
                                       do_transform_image_for_backend = True)

        conv = Converter(root_path, img_processor=img_processor, simulation=False)

        bag_list= ['train/left_right_bag_for_unittest',
                   'test/left_right_bag_for_unittest']

        (x_train, y_train), (x_test, y_test) = conv.return_rosbags(bags=bag_list)

        output_shape = img_processor.get_output_shape()
        color_scale = 'mono8' if output_shape.depth == 1 else 'bgr8'

        model_path = root_path + '/trained_data/steering_default_mono8-tf-128x72-crop_t10_b90_unittest.hdf5'

        model = RegressionModel(model_name  = 'unittest_model',
                                nn_function = '_steering_htw',
                                color_scale = color_scale,
                                img_cols    = output_shape.width,
                                img_rows    = output_shape.height,
                                img_depth   = output_shape.depth,
                                model_path  = model_path,
                                verbose     = 1,
                                training    = False)

        file_writer = FileWriter()

        filename = 'vis_act_unittest'
        file_path = root_path + '/vis/layer/' + filename + '_prediction.png'

        vis_processor = VisualizationProcessor(model=model.model, file_writer=file_writer)
        vis_processor.visualize_activation(image=x_train[0], file_name=filename)

        self.assertTrue( os.path.isfile( file_path ))

        os.remove(file_path)

    def test_visualize_saliency(self):
        img_processor = ImageProcessor(img_width = 128, img_height = 72,
                                       crop_top = 10, crop_bottom = 90,
                                       do_grayscale_image = True,
                                       do_resize_image    = True,
                                       do_crop_image      = True,
                                       do_normalize_image = True,
                                       do_transform_image_for_backend = True)

        conv = Converter(root_path, img_processor=img_processor, simulation=False)

        bag_list= ['train/left_right_bag_for_unittest',
                   'test/left_right_bag_for_unittest']

        (x_train, y_train), (x_test, y_test) = conv.return_rosbags(bags=bag_list)

        output_shape = img_processor.get_output_shape()
        color_scale = 'mono8' if output_shape.depth == 1 else 'bgr8'

        model_path = root_path + '/trained_data/steering_default_mono8-tf-128x72-crop_t10_b90_unittest.hdf5'

        model = RegressionModel(model_name  = 'unittest_model',
                                nn_function = '_steering_htw',
                                color_scale = color_scale,
                                img_cols    = output_shape.width,
                                img_rows    = output_shape.height,
                                img_depth   = output_shape.depth,
                                model_path  = model_path,
                                verbose     = 1,
                                training    = False)

        file_writer = FileWriter()

        filename = 'vis_act_unittest'
        file_path = root_path + '/vis/layer/' + filename + '_prediction.png'

        vis_processor = VisualizationProcessor(model=model.model, file_writer=file_writer)
        vis_processor.visualize_saliency(image=x_train[0], file_name=filename)

        self.assertTrue( os.path.isfile( file_path ))

        os.remove(file_path)

if __name__ == '__main__':
    unittest.main()
