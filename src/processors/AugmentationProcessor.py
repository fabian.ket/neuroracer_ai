#!/usr/bin/env python
from cv2 import cvtColor, resize, COLOR_BGR2GRAY, INTER_CUBIC

import numpy as np
from keras.preprocessing.image import ImageDataGenerator
from functools import partial
import os
import time

from multiprocessing import cpu_count
from pymp import Parallel, shared

import cv2
import yaml

MAX_NB_CPU = 8 # limit maximum number of processes to use

class AugmentationProcessor:
    '''
    Class supports standalone augmentation
    '''

    def __init__(self, x, y):
        '''
        Constructor

        :param x: np.array of images
        :param y: np.array of y values
        '''
        self.x = x
        self.y = y

        # limit amount of used processes
        self.nb_cpu = cpu_count() if cpu_count() < MAX_NB_CPU else MAX_NB_CPU

    def save_to_dir(self, path, dirname, prefix='train', img_count=2500):
        '''
        Saves the augmented images and y_vales in the given directory, saves y_values
        as yaml file, same way as the Converter.py

        :param path:      String root directory which contains dir training_data
        :param dirname:   String dirname of the folder for the augmented data
        :param prefix:    String to decide if training or test data
        :param img_count: Int number of genereted augmented images

        :raise IOError if path is not a directory

        data_gen.flow(save_to_dir="...") is not used because, now we can use a key
        as filename and as key for the yaml file

        the yaml file contains some strange values, but yaml.load()
        can read this stuff, the shared.dict() does not work exactly
        like a normal dict
        '''

        if not os.path.isdir(path):
            raise IOError('No such dir: {}'.format( path ))
        else:
            path = os.path.abspath(path) + '/training_data/data/{}/{}'.format(prefix, dirname)
            os.makedirs(path)

        data_gen = ImageDataGenerator(
            featurewise_center=False,  # set input mean to 0 over the data set
            samplewise_center=False,  # set each sample mean to 0
            featurewise_std_normalization=False,  # divide inputs by std of the data set
            samplewise_std_normalization=False,  # divide each input by its std
            zca_whitening=False,  # apply ZCA whitening
            rotation_range=20,  # randomly rotate data in the range (degrees, 0 to 180)
            width_shift_range=0.1,  # randomly shift data horizontally (fraction of total width)
            # height_shift_range=0.1,  # randomly shift data vertically (fraction of total height)
            shear_range=0.2,
            zoom_range=0.2,
            channel_shift_range=0.5,
            fill_mode='nearest',
            rescale=None,
            preprocessing_function=None)

        data_gen.fit(self.x)

        i = 1
        batch_size = img_count
        if self.x.shape[0] < img_count:
            i = int(img_count / self.x.shape[0])
            batch_size = self.x.shape[0]

        y_yaml = shared.dict()

        with Parallel(self.nb_cpu) as p:
            for j in p.xrange(i):
                x_aug_batch, y_aug_batch = data_gen.flow(self.x, self.y, batch_size=img_count).next()

                # only used to make the key absolute unique
                k = 0
                for x_aug, y_aug in zip(x_aug_batch, y_aug_batch):
                    key = '{}_{}_{}_{}'.format( time.time(), p.thread_num, j, k)
                    filename = '{}/{}.jpg'.format(path, key)

                    cv2.imwrite(filename, x_aug)
                    with p.lock:
                        y_yaml[key] = {
                            'steering_angle': y_aug[0],
                            'speed': y_aug[1]
                        }

                    k += 1

        # important, has to be done, now yaml.dump() can save the dict
        y_yaml = y_yaml.copy()

        filename = '{}/{}.yaml'.format( path, dirname )
        with open(filename, 'w') as outfile:
            yaml.dump(y_yaml, outfile)


def _main():
    image = cv2.imread('maxresdefault.jpg')
    image_count = 10
    x_train = np.array([image for x in xrange(image_count)])
    y_train = np.array([[x * 3, x * 7] for x in xrange(image_count)])

    aug_processor = AugmentationProcessor(x_train, y_train)
    aug_processor.save_to_dir('temp', 50)

    with open('temp/aug_50.yaml', 'r') as data:
        test = yaml.load(data)

if __name__ == '__main__':
    _main()
