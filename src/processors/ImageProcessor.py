#!/usr/bin/env python
from yaml import load
from cv2 import flip, resize, INTER_CUBIC, cvtColor, COLOR_BGR2GRAY
from os import path
from posixpath import normpath

import numpy as np
from keras import backend as k

from multiprocessing import cpu_count
from pymp import Parallel, shared

from collections import namedtuple

MAX_NB_CPU = 8 # limit maximum number of processes to use

def flip_horizontal(image):
    '''
    Uses cv2.flip to flip the image horizontal

    :param image: np.array/cv2.Mat the image

    :return np.array/cv2.Mat flipped image
    '''

    return flip(image, 0)


def flip_vertical(image):
    '''
    Uses cv2.flip to flip the image vertical

    :param image: np.array/cv2.Mat the image

    :return np.array/cv2.Mat flipped image
    '''

    return flip(image, 1)


def flip_horizontal_and_vertical_dual(image):
    '''
    Uses cv2.flip to flip the image horizontal and vertical

    :param image: np.array/cv2.Mat the image

    :return np.array/cv2.Mat flipped image
    '''

    return flip(image, -1)


class ImageProcessor:
    '''
    Class to do the image preprocessing
    '''

    def __init__(self,
                 img_width=480, img_height=270,
                 crop_top=0, crop_bottom=100,
                 do_grayscale_image=False,
                 do_resize_image=False,
                 do_crop_image=False,
                 do_normalize_image=False,
                 do_transform_image_for_backend=False):

        '''
        Constructor

        :param img_width:   Int width for the image resize
        :param img_height:  Int height for the image resize
        :param crop_top:    Float percent for top cropping, example crop_top=10
                                  crop 10 percent of the image top
        :param crop_bottom: Float percent for bottom cropping, example crop_bottom=90
                                  crop 10 percent of the image bottom
        :param do_grayscale_image: Bool if true execute_img_processing() does grayscaling
        :param do_resize_image:    Bool if true execute_img_processing() does resizing
        :param do_normalize_image: Bool if true execute_img_processing() does normalizing
        :param do_transform_image_for_backend: Bool if true execute_img_processing() does transforming
        '''

        self.img_width = img_width
        self.img_height = img_height

        self.crop_top = crop_top
        self.crop_bottom = crop_bottom

        self.do_grayscale_image = do_grayscale_image
        self.do_resize_image = do_resize_image
        self.do_crop_image = do_crop_image
        self.do_normalize_image = do_normalize_image
        self.do_transform_image_for_backend = do_transform_image_for_backend

        # limit amount of used processes
        self.nb_cpu = cpu_count() if cpu_count() < MAX_NB_CPU else MAX_NB_CPU

    def get_output_shape(self):
        '''
        Returns the output shape of processed images as a namedtuple

        :return namedtuple Shape with attributes width, height and depth
        '''

        Output_Shape = namedtuple('Shape', ['width', 'height', 'depth'])

        width = self.img_width
        height = self.img_height if not self.do_crop_image else self.get_cropped_height()
        depth = 1 if self.do_grayscale_image else 3

        return Output_Shape(width, height, depth)

    def _img_list_threader(self, images, func):
        '''
        Processed ImageProcessor methods multiprocessed

        :param images: List<np.array<float>> a list of images which should be processed
        :param func:   Function which gets an image and returns and image

        :return np.array of the processed images
        '''

        shared_list = shared.list()

        with Parallel(self.nb_cpu) as p:
            for image in p.iterate(images):
                image = func(image)
                with p.lock:
                    shared_list.append(image)

        return np.asarray(shared_list)

    def get_cropped_height(self):
        '''
        Retruns the image height after the cropping process

        :return Int the new image height
        '''

        start_y = int((float(self.img_height) / 100.00) * float(self.crop_top))
        end_y = int((float(self.img_height) / 100.00) * float(self.crop_bottom))

        return end_y - start_y

    def crop_img(self, image):
        '''
        Cropped the given image with the set crop_top and crop_height values

        :param image: np.array/cv2.Mat image to crop

        :retrun np.array the cropped image
        '''

        image = np.array(image).astype('float32')

        # height
        height = image.shape[0]
        start_y = int((float(height) / 100.00) * float(self.crop_top))
        end_y = int((float(height) / 100.00) * float(self.crop_bottom))

        # to prevent broken data
        if start_y < 0 or end_y <= 0 or start_y >= end_y:
            raise ValueError(
                'Cropping image not possible. Wrong Proportions: start_y=%s, end_y=%s' % (start_y, end_y))

        # width
        width = image.shape[1]
        start_x = 0
        end_x = int(width)

        return image[start_y:end_y, start_x:end_x]

    def crop_img_list(self, images):
        '''
        Cropped image lists with the set crop_top and crop_height
        uses multithreading

        :param images: List<np.array> images to crop

        :return np.array of cropped images
        '''

        return self._img_list_threader(images, self.crop_img)

    def transform_img_for_backend(self, image_data):
        '''
        Reshapes image for the used backend (mostly Tensorflow)

        :param image_data: cv2.Mat/np.array image to transform

        :return np.array the transformed image
        '''

        image_data = np.array(image_data).astype('float32')

        height = image_data.shape[0]
        width = image_data.shape[1]
        depth = image_data.shape[2] if len(image_data.shape) > 2 else 1

        if k.image_dim_ordering() == 'tf':
            # Tensorflow-Backend
            # image_data = np.reshape(image_data, (height, self.img_width, self.img_depth))
            image_data = np.reshape(image_data, (height, width, depth))
        else:
            # Theano-Backend:
            # image_data = np.reshape(image_data, (self.img_depth, height, self.img_width))
            # image_data = np.reshape(image_data, (depth, height, width))
            pass

        return image_data

    def transform_img_list_for_backend(self, images):
        '''
        Transforms a list of images for the used backend (mostly Tensorflow)
        uses multithreaded

        :param images: List<np.array> the images to transform

        :return np.array the list of transformed images
        '''

        return self._img_list_threader(images, self.transform_img_for_backend)

    def resize_img(self, image):
        '''
        Resized the given image with the set img_width and img_height

        :param image: np.array the image to resize

        :return np.array the resized image
        '''

        return resize(image,
                      (self.img_width, self.img_height),
                      interpolation=INTER_CUBIC)

    def resize_img_list(self, images):
        '''
        Resized the given image list with the set img_width and img_height
        uses multithreading

        :param images: List<np.array> images to resize

        :return np.array list of resiezed images
        '''

        return self._img_list_threader(images, self.resize_img)

    def normalize_img(self, image):
        '''
        Normalized the given image

        :param image: np.array the image to normalize

        :return np.array the normalized image
        '''

        image = np.array(image).astype('float32')
        image /= 255.
        return image

    def normalize_img_list(self, images):
        '''
        Normalized the given image list
        uses multithreading

        :param images: List<np.array> images to normalize

        :return np.array the normalized images
        '''

        return self._img_list_threader(images, self.normalize_img)

    def denormalize_img(self, image):
        '''
        Denormalized the given image

        :param image: np.array the image to denormalize

        :return np.array the denormalized image
        '''

        image = np.array(image).astype('float32')
        image *= 255.
        return image

    def denormalize_img_list(self, images):
        '''
        Denormalized the given image list
        uses multithreading

        :param images: List<np.array> images to denormalize

        :return np.array the denormalized images
        '''

        return self._img_list_threader(images, self.denormalize_img)

    def grayscale_img(self, image):
        '''
        Grayscaled the given image

        :param image: np.array image to grayscale

        :return np.array of the grayscaled image
        '''
        image = np.array(image)
        image = cvtColor(image, COLOR_BGR2GRAY) if image.shape[2] > 1 else image

        return image

    def grayscale_img_list(self, images):
        '''
        Grayscaled the given image list

        :param images: List<np.array> images to grayscale

        :return np.array of the grayscaled images
        '''

        return self._img_list_threader(images, self.grayscale_img)


    def execute_img_processing(self, image):
        '''
        Does all the image processing at ones, based on the set ImageProcessor bool states
        The order is the following: 1. grayscaling
                                    2. resizing
                                    3. cropping
                                    4. normalizing
                                    5. transforming for backend

        :param image: np.array the image to process

        :return np.array of processed image
        '''

        if self.do_grayscale_image:
            image = self.grayscale_img(image)

        if self.do_resize_image:
            image = self.resize_img(image)

        if self.do_crop_image:
            image = self.crop_img(image)

        if self.do_normalize_image:
            image = self.normalize_img(image)

        if self.do_transform_image_for_backend:
            image = self.transform_img_for_backend(image)

        return image

    def execute_img_list_processing(self, images):
        '''
        Does all the image processing at ones for a img list, based on the set ImageProcessor bool states
        uses multithreading

        :param images: List<np.array> image list to process

        :return np.array processed images
        '''

        return self._img_list_threader(images, self.execute_img_processing)

    @staticmethod
    def flip_horizontal(image):
        # Wrapper to avoid modifying python path
        return flip_horizontal(image)

def _main():
    pass

if __name__ == '__main__':
    _main()
