#!/usr/bin/env python
from time import time

import numpy as np
from vis.utils.utils import find_layer_idx
from vis.visualization import visualize_activation, visualize_saliency

class VisualizationProcessor:
    '''
    Visualizing class for activation and saliency
    '''

    def __init__(self, model=None, file_writer=None):
        '''
        Constructor

        :param model:       Keras model whick should be used for visualization
        :param file_writer: FileWriter for file logging
        '''

        self.model = model
        self.file_writer = file_writer

    def visualize_activation(self, image, file_name=None, layer_name='prediction'):
        '''
        Visualized the activation for the given layer_name

        :param image:      np.array input image for the visualization
        :param file_name:  String filename for the visualization file, if None timestamp will be used
        :param layer_name: String layer which should be visualized
        '''

        seed_input = np.array(image).reshape(1, image.shape[0], image.shape[1], image.shape[2]).astype('float32')

        layer_idx = find_layer_idx(self.model, layer_name)

        img = visualize_activation(self.model, layer_idx,
                                   filter_indices=None,
                                   backprop_modifier='guided',
                                   seed_input=seed_input)

        if not file_name:
            file_name = int(round(time() * 1000))
        else:
            file_name = file_name

        if self.file_writer is not None:
            self.file_writer.write_image(np.squeeze(img), 'vis/layer', '%s_%s' % (file_name, layer_name))

    def visualize_saliency(self, image, file_name=None, layer_name='prediction'):
        '''
        Visualized the saliency for the given layer_name

        :param image:      np.array input image for the visualization
        :param file_name:  String filename for the visualization file, if None timestamp will be used
        :param layer_name: String layer which should be visualized
        '''

        seed_input = np.array(image).reshape(1, image.shape[0], image.shape[1], image.shape[2]).astype('float32')

        layer_idx = find_layer_idx(self.model, layer_name)

        img = visualize_saliency(self.model, layer_idx,
                                 filter_indices=None,
                                 backprop_modifier='guided',
                                 seed_input=seed_input)

        if not file_name:
            file_name = int(round(time() * 1000))
        else:
            file_name = file_name

        if self.file_writer is not None:
            self.file_writer.write_image(np.squeeze(img), 'vis/layer', '%s_%s' % (file_name, layer_name))
