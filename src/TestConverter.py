#!/usr/bin/env python

from utils.Converter import *
from processors.ImageProcessor import *
import unittest
import numpy as np
import os, shutil

root_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

class TestConverter(unittest.TestCase):

    def test_return_rosbags_as_mono8_ndarray(self):
        img_processor = ImageProcessor(img_width=128, img_height=72,
                                       crop_top=0, crop_bottom=100,
                                       do_grayscale_image=True,
                                       do_resize_image=True,
                                       do_crop_image=False,
                                       do_normalize_image=True,
                                       do_transform_image_for_backend=True)

        conv = Converter(root_path, img_processor=img_processor, simulation=False)

        bag_list= ['train/left_right_bag_for_unittest',
                   'test/left_right_bag_for_unittest']

        (x_train, y_train), (x_test, y_test) = conv.return_rosbags(bags=bag_list)

        self.assertIsInstance(x_train, np.ndarray)
        self.assertIsInstance(y_train, np.ndarray)
        self.assertIsInstance(x_test,  np.ndarray)
        self.assertIsInstance(y_test,  np.ndarray)

        self.assertTupleEqual(x_train.shape, (702, 72, 128, 1))
        self.assertTupleEqual(y_train.shape, (702, 2))
        self.assertTupleEqual(x_test.shape,  (702, 72, 128, 1))
        self.assertTupleEqual(y_test.shape,  (702, 2))

    def test_convert_rosbags_to_data(self):

        conv = Converter(root_path, img_processor=None, simulation=False)

        bag_list= ['train/left_right_bag_for_unittest',
                   'test/left_right_bag_for_unittest']

        conv.convert_rosbags_to_data(bags=bag_list)

        train_path = root_path + '/training_data/data/train/left_right_bag_for_unittest'
        test_path  = root_path + '/training_data/data/test/left_right_bag_for_unittest'

        train_list = os.listdir(train_path)
        test_list  = os.listdir(test_path)

        self.assertTrue( len(train_list) == 703 )
        self.assertTrue( len(test_list)  == 703 )

        try:
            shutil.rmtree(train_path)
            shutil.rmtree(test_path)
        except Exception as e:
            print(e)

    def test_path_fail(self):
        with self.assertRaises(IOError):
            conv = Converter('', img_processor=None, simulation=False)

    def test_path_exists_but_no_files_or_dirs(self):
        conv = Converter('/', img_processor=None, simulation=False)

        with self.assertRaises(OSError):
            conv.return_rosbags()

    def test_return_data_as_bgr8_ndarray(self):
        img_processor = ImageProcessor(img_width=128, img_height=72,
                                       crop_top=0, crop_bottom=100,
                                       do_grayscale_image=False,
                                       do_resize_image=True,
                                       do_crop_image=False,
                                       do_normalize_image=True,
                                       do_transform_image_for_backend=True)

        conv = Converter(root_path, img_processor=img_processor, simulation=False)

        bag_list= ['train/left_right_bag_for_unittest',
                   'test/left_right_bag_for_unittest']

        conv.convert_rosbags_to_data(bags=bag_list)

        dir_list= ['train/left_right_bag_for_unittest',
                   'test/left_right_bag_for_unittest']

        (x_train, y_train), (x_test, y_test) = conv.return_data(dirs=dir_list)

        train_path = root_path + '/training_data/data/train/left_right_bag_for_unittest'
        test_path  = root_path + '/training_data/data/test/left_right_bag_for_unittest'

        self.assertIsInstance(x_train, np.ndarray)
        self.assertIsInstance(y_train, np.ndarray)
        self.assertIsInstance(x_test,  np.ndarray)
        self.assertIsInstance(y_test,  np.ndarray)

        self.assertTupleEqual(x_train.shape, (702, 72, 128, 3))
        self.assertTupleEqual(y_train.shape, (702, 2))
        self.assertTupleEqual(x_test.shape,  (702, 72, 128, 3))
        self.assertTupleEqual(y_test.shape,  (702, 2))

        try:
            shutil.rmtree(train_path)
            shutil.rmtree(test_path)
        except Exception as e:
            print(e)

    def test_return_rosbags_without_img_processing(self):
        conv = Converter(root_path, img_processor=None, simulation=False)

        bag_list= ['train/left_right_bag_for_unittest',
                   'test/left_right_bag_for_unittest']

        (x_train, y_train), (x_test, y_test) = conv.return_rosbags(bags=bag_list)

        self.assertIsInstance(x_train, np.ndarray)
        self.assertIsInstance(y_train, np.ndarray)
        self.assertIsInstance(x_test,  np.ndarray)
        self.assertIsInstance(y_test,  np.ndarray)

        self.assertTupleEqual(x_train.shape, (702, 360, 640, 3))
        self.assertTupleEqual(y_train.shape, (702, 2))
        self.assertTupleEqual(x_test.shape,  (702, 360, 640, 3))
        self.assertTupleEqual(y_test.shape,  (702, 2))


if __name__ == '__main__':
    unittest.main()
