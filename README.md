Fork of the NeuroRace project https://gitlab.com/NeuroRace/neuroracer-ai
This source code is based on code of 02.01.2018, the fork was created on 09.03.2018 but the code was exchanged

This project needs the neuroracer_utils project to work:

Needed files from neuroracer_utils are:

- YamlLoader
- CvBridgeHandler

The directory structur has to stay as it is

Example:

root-dir: /root

In /root are the following directories:

- /root/neuroracer_ai
- /root/neuroracer_robot
- /root/neuroracer_utils

Every neuroracer_* directory structure has to stay the same as it was created after the clone.
The reason is, that neuroracer_ai uses files from the neuroracer_util project and
neuroracer_robot uses files from neuroracer_ai (in neurorace_ai_wrapper) and also files
from neuroracer_utils

Don't forget to execute setup.py to create needed folders.