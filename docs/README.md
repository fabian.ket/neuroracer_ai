### Note
The paper is not up to date. A revision is in process. 

The following main changes have been applied:
* Canny Edge Detection has been changed to raw greyscale
* Line of reference has been removed. Raw pixel data is used (end-to-end concept)
* An additonal neural network approach has been added. The former one has been improved.
* Process optimization of training and autonomous unit
* Design and structure change of processing 

Also further not listed improvements and additions were made as well as some are still in work.


Class diagram xml created with https://www.draw.io/

Diagram of 09.03.2018